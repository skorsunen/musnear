import time,datetime,sys
if sys.version_info[0] < 3:
    reload(sys)
    sys.setdefaultencoding('utf-8')
import re
import logging
import random
import requests
from lxml import html
import base64
import json

logger = logging.getLogger(__name__)
#GET USER-AGENT
def getUserAgent():
    platform = random.choice(['Macintosh', 'Windows', 'X11'])
    if platform == 'Macintosh':
        osc  = random.choice(['68K', 'PPC'])
    elif platform == 'Windows':
        osc  = random.choice(['Win3.11', 'WinNT3.51', 'WinNT4.0', 'Windows NT 5.0', 'Windows NT 5.1', 'Windows NT 5.2', 'Windows NT 6.0', 'Windows NT 6.1', 'Windows NT 6.2', 'Win95', 'Win98', 'Win 9x 4.90', 'WindowsCE'])
    elif platform == 'X11':
        osc  = random.choice(['Linux i686', 'Linux x86_64'])
    browser = random.choice(['chrome', 'firefox', 'ie'])
    if browser == 'chrome':
        webkit = str(random.randint(500, 599))
        version = str(random.randint(0, 24)) + '.0' + str(random.randint(0, 1500)) + '.' + str(random.randint(0, 999))
        return 'Mozilla/5.0 (' + osc + ') AppleWebKit/' + webkit + '.0 (KHTML, live Gecko) Chrome/' + version + ' Safari/' + webkit
    elif browser == 'firefox':
        currentYear = datetime.date.today().year
        year = str(random.randint(2000, currentYear))
        month = random.randint(1, 12)
        if month < 10:
            month = '0' + str(month)
        else:
            month = str(month)
        day = random.randint(1, 30)
        if day < 10:
            day = '0' + str(day)
        else:
            day = str(day)
        gecko = year + month + day
        version = random.choice(['1.0', '2.0', '3.0', '4.0', '5.0', '6.0', '7.0', '8.0', '9.0', '10.0', '11.0', '12.0', '13.0', '14.0', '15.0'])
        return 'Mozilla/5.0 (' + osc + '; rv:' + version + ') Gecko/' + gecko + ' Firefox/' + version
    elif browser == 'ie':
        version = str(random.randint(1, 10)) + '.0'
        engine = str(random.randint(1, 5)) + '.0'
        option = random.choice([True, False])
        if option == True:
            token = random.choice(['.NET CLR', 'SV1', 'Tablet PC', 'Win64; IA64', 'Win64; x64', 'WOW64']) + '; '
        elif option == False:
            token = ''
        return 'Mozilla/5.0 (compatible; MSIE ' + version + '; ' + osc + '; ' + token + 'Trident/' + engine + ')'

def city_by_ip(ip):
    city = ""
    region =""
    country =""
    loc = ""
    try:
        query="http://ipinfo.io/" + ip + "/json"
        headers = {'User-agent': getUserAgent()}
        response = requests.get(query, headers=headers, timeout=10)
        if response != "Error" and response.ok:
            txt = response.content.decode('utf-8')
            info = json.loads(txt)
            city = info.get('city','')
            region = info.get('region','')
            country = info.get('country','')
            loc = info.get('loc','')
    except Exception as ex:
        print("city_by_ip",ex)
    return city,region,country,loc

def proxylist():
    primary_url = "http://proxy-list.org/english/index.php?p="
    urls = []
    for i in range(1, 11):
        urls.append(primary_url + str(i))
    ip_lst = []
    try:
        for url in urls:
            headers ={'User-agent': getUserAgent()}
            response = requests.get(url, headers=headers,timeout=35)
            if response != 'Error' and response.ok:
                contents=response.content.decode('utf-8','ignore')
                tree = html.fromstring(contents)
                lis = tree.xpath('//li[@class="proxy"]//text()')

                for li in lis:
                    if "Proxy(" in li:
                        ip = li[7:-1]
                        ip_lst.append(base64.b64decode(ip).decode('utf-8'))

    except Exception as ex:
        logger.error("proxylist %s",str(ex))
    return ip_lst

def nntime():
    headers ={'User-agent': getUserAgent()}
    url = "http://nntime.com/"
    page1 = "proxy-updated-01.htm"
    pip = []
    try:
        response = requests.get(url+page1, headers=headers,timeout=35)
        if response != 'Error' and response.ok:
            contents=response.content.decode('utf-8','ignore')
            tree = html.fromstring(contents)
            pages = tree.xpath('//div[@id="navigation"]//a/@href')[0:-1]
            pages.insert(0,page1)
            for page in pages:
                response = requests.get(url+page, headers=headers,timeout=35)
                if response == 'Error' or not response.ok: continue
                contents=response.content.decode('utf-8','ignore')
                decoder_string = re.findall(r'<script type="text/javascript">\n(.*?)</script>', contents)
                decoderls = decoder_string[0].split(";")
                temp_tuple = []
                for itm in decoderls:
                    if itm:
                        temp_tuple.append((itm.split("=")))
                decoder_dict = dict(temp_tuple)
                ips = re.findall(r'></td><td>(.*?)<script type="text/javascript">document', contents)
                ports = []
                templs = re.findall(r'<script type="text/javascript">.*?</script>', contents)
                for e,line in enumerate(templs):
                    temp = line.replace('<script type="text/javascript">document.write(":"+', '')
                    temp = temp.replace(')</script>', '')
                    codes = temp.split("+")
                    temp_port = ""
                    for code in codes:
                        temp_port += decoder_dict[code]
                    ips[e] += ":" + temp_port
                pip.extend(ips)
#                print(len(pip))
    except Exception as ex:
        logger.error("nntime %s",str(ex))
    return pip

def usproxy():
#    print("Grabbing: http://www.us-proxy.org/")
    templs = []
    url = "http://www.us-proxy.org/"
    headers ={'User-agent': getUserAgent()}
    try:
        response = requests.get(url, headers=headers,timeout=35)
        if response != 'Error' and response.ok:
            contents=response.content.decode('utf-8','ignore')
            templs = re.findall(r'<tr><td>(.*?)</td><td>', contents)
            templs2 = re.findall(r'</td><td>[1-99999].*?</td><td>', contents)
            for i in range(len(templs)):
                templs[i] = templs[i] + ":" + templs2[i].replace('</td><td>', '')
#        print(len(templs))
    except Exception as ex:
        logger.error("usproxy %s",str(ex))
    return templs

checked = {}
def get_free_proxy():
    i = 0
    pip = proxylist()
  #  if len(pip) < 2:
    pip += nntime()
    if len(pip) < 2:
        pip += usproxy()
    random.shuffle(pip)

    for e,p in enumerate(pip):
        if p in checked:
            if checked[p] == None:
                continue
            else:
                return checked[p]
        else:
            checked[p] = None
        proxies = {"http": "http://" + p +"/","https": "http://" + p +"/"}
        headers ={'User-agent': getUserAgent()}
        url = "http://bandsintown.com"
        try:
            start_time = time.time()
            response = requests.get(url, headers=headers,proxies=proxies,timeout=10)
            if response != 'Error' and response.ok:
                contents = response.content.decode('utf-8')
                if "artist" in contents :
                    checked[p] = proxies
                    ip = p.split(":")[0]
                    city, region, country, loc = city_by_ip(ip)
                    print(city,country)
                    return proxies
        except Exception as ex:
          #  logger.error("check_free_proxy %s",str(ex))
            pass
    return None