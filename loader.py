import pycountry
from proxy import *
from musdb import *
from musutils import toint,cmp,dict_merge,tofloat
import logging
import re
import facebook
import facebook_bot

import dateutil.parser as dparser
app_id = "1511153822490000"
secret = "cc8306977314cd43e1e1bde2b3971d18"
fb_token="1511153822490065|sYlg7E5gKF0yC_y2hjVkXzwcOiu"
headers_0 = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0',
              'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
              'Accept-Language': 'en-US,en;q=0.5',
              'Accept-Encoding': 'gzip, deflate',
              'Connection': 'keep-alive',
              'Cache-Control': 'max-age=0'}

def fb_search():
    graph = facebook.GraphAPI(access_token=fb_token, version="2.10")
    # Search for places near 1 Hacker Way in Menlo Park, California.
    places = graph.search(type='place',
                          center='37.4845306,-122.1498183',
                          fields='name,location')

    print(places)
    # Each given id maps to an object the contains the requested fields.
    for place in places['data']:
        print('%s %s' % (place['name'].encode(), place['location'].get('zip')))
    # Search for a user named "Mark Zuckerberg" and show their ID and name.
    # users = graph.search(type='user', q='Mark Zuckerberg')
    #
    # for user in users['data']:
    #     print('%s %s' % (user['id'], user['name'].encode()))


def fb_events():
    graph = facebook.GraphAPI(access_token=fb_token, version="2.10")
    events = graph.request("/search?q=Poetry&type=event&limit = 100")
    print events


def get_fb_token(app_id, app_secret):
    payload = {'grant_type': 'client_credentials', 'client_id': app_id, 'client_secret': app_secret}
    file = requests.post('https://graph.facebook.com/oauth/access_token?', params = payload)
   # print file.text #to test what the FB api responded with
    result = file.json()['access_token']
    #print file.text #to test the TOKEN
    return result


_evn_venue_id = re.compile('(?:event|venue)\/(\d+)', re.IGNORECASE)
def get_evn_venue_id(lnk):
    result = _evn_venue_id.findall(lnk)
    if len(result) > 0:
        return result[0]

_artist_id = re.compile('bandsintown.com\/(\w+)?', re.IGNORECASE)
def get_artist_id(lnk):
    result = _artist_id.findall(lnk)
    if len(result) > 0:
        return result[0]


_map_coordinates = re.compile('ll=(.+)\%2C(.+)\&q', re.IGNORECASE)
def get_map_coordinates(lnk):
    #Get longitude and latitude
    result = _map_coordinates.findall(lnk)
    if len(result) > 0 and len(result[0]) > 1:
        return tofloat(result[0][1]),tofloat(result[0][0])
    return None,None


def add_artist(artist):
    rows = []
    artist_guid = ""
    if type(artist.get("artist_id", "")) is int:
        sql = "SELECT * from artists WHERE bit_artist_id=%s"
        params = [artist["artist_id"]]
        rows = select_query(sql, params)

    params = []
    if len(rows) > 0:
        # The artist is existed
        artist_guid = rows[0]["artist_guid"]
        sql = ""
        if artist["artist_name"] != "" and artist["artist_name"] != rows[0]["artist_name"]:
            if sql != "": sql += ","
            sql += r"artist_name=%s"
            params.append(artist["artist_name"])
        if artist["artist_id"] != rows[0]["bit_artist_id"]:
            if sql != "": sql += ","
            sql += r"bit_artist_id=%s"
            params.append(artist["artist_id"])
        if sql != "":
            dt = datetime.datetime.now()
            params.append(dt)
            sql += ",date_added=%s "
            sql = "UPDATE artists SET " + sql + " WHERE artist_guid=%s"
            params.append(artist_guid)
            create_update(sql, params)
    else:
        sql = "INSERT INTO artists (artist_guid,artist_name,bit_artist_id,date_added) VALUES \
        (%s,%s,%s,%s) "
        artist_guid = str(uuid.uuid1())
        params.append(artist_guid)
        params.append(artist["artist_name"])
        params.append(artist["artist_id"])
        dt = datetime.datetime.now()
        params.append(dt)
        if not create_update(sql,params):
            return ""
    return artist_guid


def add_venue(venue):
    rows = []
    venue_guid = ""
    region = venue["venue_location"].get("region","")
    if region == None or region == venue["venue_location"].get("country","") or region.isdigit():
        venue["venue_location"]["region"] = ""
    if type(venue.get("venue_id", "")) is int:
        sql = "SELECT * from venues WHERE bit_venue_id=%s"
        params = [venue["venue_id"]]
        rows = select_query(sql, params)

    params = []
    if len(rows) > 0:
        # The venue is existed
        venue_guid = rows[0]["venue_guid"]
        sql = ""
        if venue.get("venue_name","") != "" and venue["venue_name"] != rows[0]["venue_name"]:
            if sql != "": sql += ","
            sql += r"venue_name=%s"
            params.append(venue["venue_name"])
        if venue.get("phone","") != "" and venue["phone"] != rows[0]["phone"]:
            if sql != "": sql += ","
            sql += r"phone=%s"
            params.append(venue["phone"])
        if venue["venue_location"].get("country","") != "" and venue["venue_location"]["country"] != rows[0]["country"]:
            if sql != "": sql += ","
            sql += r"country=%s"
            params.append(venue["venue_location"]["country"])
        if  venue["venue_location"].get("region","") != "" and venue["venue_location"]["region"] != rows[0]["region"]:
            if sql != "": sql += ","
            sql += r"region=%s"
            params.append(venue["venue_location"]["region"])
        if venue["venue_location"].get("city","") != "" and venue["venue_location"]["city"] != rows[0]["city"]:
            if sql != "": sql += ","
            sql += r"city=%s"
            params.append(venue["venue_location"]["city"])
        if venue["venue_location"].get("street","") != "" and venue["venue_location"]["street"] != rows[0]["street"]:
            if sql != "": sql += ","
            sql += r"street=%s"
            params.append(venue["venue_location"]["street"])
        if "longitude" in  venue["venue_location"] and "latitude" in  venue["venue_location"] and \
                (venue["venue_location"]["longitude"] != rows[0]["longlat"].x or \
                    venue["venue_location"]["latitude"] != rows[0]["longlat"].y):
            if sql != "": sql += ","
            longlat = '(%s,%s)' % (venue["venue_location"]["longitude"], venue["venue_location"]["latitude"])
            sql += r"longlat=%s"
            params.append(longlat)
        if sql != "":
            dt = datetime.datetime.now()
            params.append(dt)
            sql += ",date_added=%s "
            sql = "UPDATE venues SET " + sql + " WHERE venue_guid=%s"
            params.append(venue_guid)
            create_update(sql, params)
    else:
        add_city(venue)
        sql = "INSERT INTO venues (venue_guid,venue_name,country,region,city,street,longlat,bit_venue_id,date_added,phone) VALUES \
        (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) "
        venue_guid = str(uuid.uuid1())
        params.append(venue_guid)
        params.append(venue["venue_name"])
        params.append(venue["venue_location"]["country"])
        params.append(venue["venue_location"]["region"])
        params.append(venue["venue_location"]["city"])
        params.append(venue["venue_location"].get("street",""))
        longlat = '(%s,%s)' % (venue["venue_location"]["longitude"],venue["venue_location"]["latitude"])
        params.append(longlat)
        params.append(toint(venue["venue_id"]))
        dt = datetime.datetime.now()
        params.append(dt)
        params.append(venue.get("phone",""))
        if not create_update(sql,params):
            return ""
    return venue_guid


def add_city(event):
    rows = []
    country = event["venue_location"]["country"]
    region = event["venue_location"]["region"]
    if region == None or region == country or region.isdigit(): region = ""
    city = event["venue_location"]["city"]
    if country == None or country == "": return
    if city == None or city == "": return

    country = country.strip()
    region = region.strip()
    city = city.strip()

    try:
        abbr = pycountry.countries.lookup(country)
        country = abbr.alpha_2
    except:
        pass
    sql = "SELECT * from cities WHERE country=%s and region=%s and city=%s"
    params = [country,region,city]
    rows = select_query(sql, params)
    if len(rows) > 0 :
        return
        # if rows[0]["longlat"].x != 0 or rows[0]["longlat"].y != 0:
        #     return
        # else:
        #     longlat = '(%s,%s)' % (event["venue_location"]["longitude"], event["venue_location"]["latitude"])
        #     sql = "UPDATE cities SET longlat=%s WHERE country=%s and region=%s and city=%s"
        #     params = [longlat,country, region, city]
        #     create_update(sql, params)
    else:
        sql = "INSERT INTO cities (city_guid,country,region,city,longlat,date_added) VALUES (%s,%s,%s,%s,%s,%s)"
        longlat = '(%s,%s)' % (event["venue_location"]["longitude"], event["venue_location"]["latitude"])
        dt = datetime.datetime.now()
        city_guid = str(uuid.uuid1())
        params = [city_guid, country, region, city, longlat, dt]
        create_update(sql, params)


def add_event(event):
    rows = []
    event_guid = ""
    region = event["venue_location"].get("region","")
    if region == None or region == event["venue_location"].get("country","") or region.isdigit():
        event["venue_location"]["region"] = ""
    if type(event.get("id","")) is int:
        sql = "SELECT * from events WHERE bit_id=%s"
        params = [event["id"]]
        rows = select_query(sql,params)
    elif 'facebook_event_id' in event and event['facebook_event_id'] != None and event['facebook_event_id'] != "" :
        sql = "SELECT * from events WHERE data->>'fb_event_id'=%s"
        params = [event["facebook_event_id"]]
        rows = select_query(sql, params)
    venue_guid = add_venue(event)
    artist_guid = add_artist(event)
    params = []
    data = {"ticket_url": event.get('ticket_url', "")}
    if 'facebook_event_id' in event:
        data['fb_event_id'] = event['facebook_event_id']
    if 'formatted_location' in event:
        data['formatted_location'] = event['formatted_location']
    if 'formatted_datetime' in event:
        data['formatted_datetime'] = event['formatted_datetime']
    if "share_params" in event and "picture" in event["share_params"]:
        data["picture"] = event["share_params"]["picture"]
    if event.get("event_descr","").strip() != "":
        data["event_descr"] = event["event_descr"].strip()
    if len(rows) > 0:
        #The event is existed
        event_guid = rows[0]["event_guid"]
        sql = ""
        dt = dparser.parse(event['datetime'], fuzzy=True)
        if dt != rows[0]["event_time"]:
            if sql != "": sql += ","
            sql += r"event_time=%s"
            params.append(dt)
        if venue_guid != rows[0]["venue_guid"]:
            if sql != "": sql += ","
            sql += r"venue_guid=%s"
            params.append(venue_guid)
        if artist_guid != rows[0]["artist_guid"]:
            if sql != "": sql += ","
            sql += r"artist_guid=%s"
            params.append(artist_guid)
        flds = ["venue_name","artist_name","title"]
        for fld in flds:
            if event[fld] != "" and event[fld] != rows[0][fld]:
                if sql != "": sql += ","
                sql += fld + r"=%s"
                params.append(event[fld])
        if event["id"] != rows[0]["bit_id"]:
            if sql != "": sql += ","
            sql += r"bit_id=%s"
            params.append(event["id"])
        if event["venue_id"] != rows[0]["bit_venue_id"]:
            if sql != "": sql += ","
            sql += r"bit_venue_id=%s"
            params.append(event["venue_id"])

        if event["artist_id"] != rows[0]["bit_artist_id"]:
            if sql != "": sql += ","
            sql += r"bit_artist_id=%s"
            params.append(event["artist_id"])
        if event["venue_location"]["country"] != "" and event["venue_location"]["country"] != rows[0]["country"]:
            if sql != "": sql += ","
            sql += r"country=%s"
            params.append(event["venue_location"]["country"])
        if  event["venue_location"]["region"] != rows[0]["region"]:
            if sql != "": sql += ","
            sql += r"region=%s"
            params.append(event["venue_location"]["region"])
        if event["venue_location"]["city"] != "" and event["venue_location"]["city"] != rows[0]["city"]:
            if sql != "": sql += ","
            sql += r"city=%s"
            params.append(event["venue_location"]["city"])
        if event["venue_location"].get("street","") != "" and event["venue_location"]["street"] != rows[0]["street"]:
            if sql != "": sql += ","
            sql += r"street=%s"
            params.append(event["venue_location"]["street"])
        if event["venue_location"]["longitude"] != rows[0]["longlat"].x or \
                    event["venue_location"]["latitude"] != rows[0]["longlat"].y:
            if sql != "": sql += ","
            longlat = '(%s,%s)' % (event["venue_location"]["longitude"], event["venue_location"]["latitude"])
            sql += r"longlat=%s"
            params.append(longlat)
        if rows[0]["data"] != None:
            mdata = dict_merge(rows[0]["data"],data)
            if cmp(rows[0]["data"],mdata) == 1:
               # print(rows[0]["data"],mdata)
                if sql != "": sql += ","
                sql += r"data=%s"
                params.append(json.dumps(mdata))
        elif data != None and len(data) > 0:
            sql += r"data=%s"
            params.append(json.dumps(data))
        if sql != "":
            dt = datetime.datetime.now()
            params.append(dt)
            sql += ",date_added=%s "
            sql = "UPDATE events SET " + sql + " WHERE event_guid=%s"

            params.append(event_guid)
            create_update(sql, params)
    else:
        sql = "INSERT INTO events (event_guid,event_time,venue_guid,venue_name,artist_guid,artist_name,\
        bit_id,bit_venue_id,bit_artist_id,title,country,region,city,street,longlat,data,date_added) VALUES \
         (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        event_guid = str(uuid.uuid1())
        params.append(event_guid)
        dt = dparser.parse(event['datetime'], fuzzy=True)
        params.append(dt)
        params.append(venue_guid)
        params.append(event["venue_name"])
        params.append(artist_guid)
        params.append(event["artist_name"])
        params.append(event["id"])
        params.append(event["venue_id"])
        params.append(event["artist_id"])
        params.append(event["title"])
        params.append(event["venue_location"]["country"])
        params.append(event["venue_location"]["region"])
        params.append(event["venue_location"]["city"])
        params.append(event["venue_location"].get("street",""))
        longlat = '(%s,%s)' % (event["venue_location"]["longitude"],event["venue_location"]["latitude"])
        params.append(longlat)

        params.append(json.dumps(data))
        dt = datetime.datetime.now()
        params.append(dt)
        if not create_update(sql, params):
            return ""
    return event_guid


def parse_bit_event(session,ln):
    if type(ln) is int or not ln.startswith("http"):
        ln = "http://www.bandsintown.com/event/%s" % ln
    vlnk = []
    alnk = []
    try:
        response = session.get(ln)
        if response != 'Error' and response.ok:
            contents = response.content.decode('utf-8', 'ignore')
            tree = html.fromstring(contents)
            # Venue links
            vlnk = tree.xpath("//a[contains(@href,'/venue')]/@href")
            ven_name=ven_street=ven_city=ven_region=ven_country=event_descr=""
            vn = tree.xpath("//div[@class='event-venue']//h2[@class='event-name']//text()")
            if len(vn) > 0: ven_name =  vn[0]
            vn = tree.xpath("//div[@class='event-venue']//h3[@itemprop='streetAddress']//text()")
            if len(vn) > 0: ven_street = vn[0]
            vn = tree.xpath("//div[@class='event-venue']//span[@itemprop='addressLocality']//text()")
            if len(vn) > 0: ven_city = vn[0]
            vn = tree.xpath("//div[@class='event-venue']//span[@itemprop='addressRegion']//text()")
            if len(vn) > 0: ven_region = vn[0]
            vn = tree.xpath("//div[@class='event-venue']//span[@itemprop='addressCountry']//text()")
            if len(vn) > 0: ven_country = vn[0]
            vn = tree.xpath("//div[@class='event-description' and @style='display:none;']//text()")
            if len(vn) > 0: event_descr = vn[0].replace("See Less","")
            # Artists links
            alnk = tree.xpath("//meta[@itemprop='sameAs']/@content")
            # Data fo events
            scrp = tree.xpath("//script[contains(text(),'Models.Event')]//text()")
            if len(scrp) > 0:
                a = scrp[0].find("Models.Event(") + len("Models.Event(")
                if a > 1:
                    b = scrp[0].find(");", a)
                    event = json.loads(scrp[0][a:b])
                    event.setdefault("venue_location",{})
                    if ven_name != "":
                        event["venue_name"] = ven_name
                    event["venue_location"]["street"] = ven_street
                    if event["venue_location"].get("city","") == "":
                        event["venue_location"]["city"] = ven_city
                    if event["venue_location"].get("region", "") == "":
                        event["venue_location"]["region"] = ven_region
                    if event["venue_location"].get("country", "") == "":
                        event["venue_location"]["country"] = ven_country
                    event["event_descr"] = event_descr
                    print("title", event['title'])
                    add_event(event)
    except Exception as ex:
        logging.exception("parse_bit_event %s",ex)
    vnid = [get_evn_venue_id(ln) for ln in vlnk]
    artid = [get_artist_id(ln) for ln in alnk]
    return vnid,artid


def parse_bit_venue(session,ln):
    if type(ln) is int or not ln.startswith("http"):
        ln = "http://www.bandsintown.com/venue/%s" % ln
    venue_id = int(get_evn_venue_id(ln))
    eventsid = []

    venue = {"venue_id":venue_id,"venue_location":{}}
    try:
        response = session.get(ln)
        if response != 'Error' and response.ok:
            contents = response.content.decode('utf-8', 'ignore')
            tree = html.fromstring(contents)
            eventsid = [get_evn_venue_id(ln) for ln in tree.xpath("//a[contains(@href,'/event')]/@href")]
            vn = tree.xpath("//div[@class='venue-name']//text()")
            if len(vn) > 0 and vn[0] != "":
                venue["phone"] = vn[0]
            vn = tree.xpath("//div[@class='venue-phone']//text()")
            if len(vn) > 0 and vn[0] != "":
                venue["phone"] = vn[0]
            vn = tree.xpath("//div[@class='venue-image']/a/@href")
            if len(vn) > 0 and vn[0] != "":
                lon,lat = get_map_coordinates(vn[0])
                if lon != None and lat != None:
                    venue["venue_location"]["longitude"] = lon
                    venue["venue_location"]["latitude"] = lat
            add_venue(venue)
    except Exception as ex:
        logging.exception("parse_bit_venue %s",ex)
    return eventsid


def parse_bit_artist(session,al):
    # Get events andd venues from artist's page
    if not al.startswith("http"):
        al = "http://www.bandsintown.com/" + al
    response = session.get(al)
    venuesid = []
    eventsid = []
    if response != 'Error' and response.ok:
        contents = response.content.decode('utf-8', 'ignore')
        tree = html.fromstring(contents)
        scrp = tree.xpath("//script[contains(text(),'BIT.Collections.EventList')]//text()")
        if len(scrp) > 0:
            a = scrp[0].find("BIT.Collections.EventList(") + len("BIT.Collections.EventList(")
            if a > 1:
                b = scrp[0].find(");", a)
                events = json.loads(scrp[0][a:b])
                for event in events:
                    print("--title", event['title'])
                    venuesid.append(event["venue_id"])
                    eventsid.append(event["id"])
                    add_event(event)
    return eventsid,venuesid


def parse_bit():
    base_url = "https://bandsintown.com"
    track_ev = {}
    track_ven ={}
    track_art = {}
    venlnk = []
    evlnk = []
    artlnk = []
    while 1:
        try:
            pr = get_free_proxy()
            print(pr)
            time.sleep(2)
            headers = {'User-agent': getUserAgent()}
            session = requests.Session()
            response = session.get(base_url, headers=headers, proxies=pr, timeout=10)
            if response != 'Error' and response.ok:
                contents = response.content.decode('utf-8', 'ignore')
                tree = html.fromstring(contents)
                #links for nearest events
                links = tree.xpath("//a[contains(@href,'/event')]/@href")
             #   links = [ln for ln in links if "event" in ln]
                evlnk += links
                print(links)
                while len(links) > 0:
                    for ln in links:
                        if type(ln) is int or not ln.startswith("http"):
                            evids = str(ln)
                        else:
                            evids = get_evn_venue_id(ln)
                        if evids in track_ev: continue
                        vlnk,alnk = parse_bit_event(session,ln)
                        track_ev[evids] = datetime.datetime.now()
                        venlnk += vlnk
                        artlnk += alnk
                    links = []
                    for al in artlnk:
                        #Get events from artist's page
                        if al in track_art:
                            continue
                        print("artist", al)
                        eventsid, venuesid = parse_bit_artist(session,al)
                        links += eventsid
                        venlnk += venuesid
                        track_art[al] = datetime.datetime.now()
                    artlnk = []
                    for vl in venlnk:
                        #Get events from venue page
                        if type(vl) is int or not vl.startswith("http"):
                            vnids = str(ln)
                        else:
                            vnids = get_evn_venue_id(vl)
                        if vnids in track_ven:
                            continue
                        print("venue", vl)
                        eventsid = parse_bit_venue(session,vl)
                        links += eventsid
                        track_ven[vnids] = datetime.datetime.now()
                    venlnk = []
                    links = list(set(links))

        except Exception as ex:
            logging.exception("parse_bit %s",ex)
        #     raise ex
         #   return


def fb_events_places(longitude,latitude,distance=100):
    lst = facebook_bot.get_page_ids(latitude, longitude, distance=100)
    i = 0
    for pid in lst:
        nodes = facebook_bot.get_events(pid).get(pid,{})
        if 'events' in nodes:
            data = nodes['events']['data']
            for dt in data:
                event = {}
                event["venue_location"] = {}
                event["title"] = dt.get('name', '')
                event['datetime']
                # place_info = {}
                # if "place" in dt and "id" in dt["place"]:
                #     place_info = facebook_bot.get_page_info(dt["place"]["id"],fields=facebook_bot.PLACE_FIELDS)

                print(dt.get('category',''), dt.get('name', ''),dt.get('start_time',''),dt.get('end_time',''),dt.get('place',{}).get('location',''))
                i += 1
                if i > 5:
                    break

if __name__ == '__main__':
    # s = pycountry.subdivisions.get(country_code='US')
    # t = pycountry.countries.lookup('United States')
    # print get_evn_venue_id("http://www.bandsintown.com/event/3758283-top-fuel-saloon-braidwood-il-tickets-and-schedule")
    # print get_artist_id("http://www.bandsintown.com/Elisium?came_from=178")
    #print get_map_coordinates("http://maps.google.com/?ll=50.718483%2C-1.880256&q=Bournemouth+International+Centre%2CExeter+Road%2CBournemouth%2CBH2+5%2CUnited+Kingdom&z=13")
    # register_point()
    # parse_bit()
    #print get_fb_token(app_id,secret)
    #fb_search()
    #fb_events()

 #   s = facebook_bot.get_events(164606950371187, base_time='2017-05-07')
   fb_events_places( 6.1053,50.7708)
   # dt = dparser.parse('2017-11-18T15:00:00-0500',fuzzy=True)
  #  create_json_indexes()
    # lst = facebook_bot.get_page_ids(40.763871, -73.979904, distance=100)
    # i = 0
    # for pid in lst:
    #     nodes = facebook_bot.get_events(pid).get(pid,{})
    #     if 'events' in nodes:
    #         print(nodes)
    #         i += 1
    #         if i > 5:
    #             break