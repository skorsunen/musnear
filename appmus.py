#-------------------------------------------------------------------------------
# Name:        appvia.py
# Purpose:    Web interface for via-transfer.com
#
# Author:      Serge
#
# Created:     07/10/2017
#-------------------------------------------------------------------------------
import tornado.auth
import tornado.escape
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado.options import define, options
from tornado.web import URLSpec as URL
from tornado import gen
from tornado.queues import Queue
import os,sys
if sys.version_info[0] < 3:
    reload(sys)
    sys.setdefaultencoding('utf-8')
import shutil
import urllib
from time import sleep,time,strftime,gmtime,strptime
import json
import unicodedata
import datetime
import math
import logging
import momoko
from musdb import *
from asyncutils import *
from lxml import html
import pycurl


if os.name != "nt":
    AsyncHTTPClient.configure("tornado.curl_httpclient.CurlAsyncHTTPClient")
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
handler = logging.FileHandler('appmus.log')
handler.setLevel(logging.INFO)
### create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
### add the handlers to the logger
logger.addHandler(handler)
DEBUG = True
DIRNAME = os.path.dirname(__file__)
STATIC_PATH = os.path.join(DIRNAME, 'static')
TEMPLATE_PATH = os.path.join(DIRNAME, 'templates')
COOKIE_SECRET = '6XdWEBuWSm+sqlJSC3QLp70lR5USlkdFmkOdChZWOPm='
prefix = ""
define("port", default=8781, help="run on the given port", type=int)

dateformats ={'fr':'%d/%m/%Y','en-AU':'%d/%m/%Y','en-GB':'%d/%m/%Y',
              'en-NZ':'%d/%m/%Y','de':'%d.%m.%Y','ca':'%d/%m/%Y',
              'da':'%d-%m-%Y','fr-CA':'%Y-%m-%d','fr-CH':'%d.%m.%Y',
              'it':'%d/%m/%Y','it-CH':'%d.%m.%Y','ja':'%y/%m/%d',
              'nl':'%d-%m-%Y','nl-BE':'%d/%m/%Y','nn':'%d.%m.%Y',
              'no':'%d.%m.%Y','pl':'%d.%m.%Y','pt':'%d/%m/%Y',
              'pt-BR':'%d/%m/%Y','rm':'%d/%m/%Y','ro':'%d.%m.%Y',
              'ru':'%d.%m.%Y','sk':'%d.%m.%Y','sl':'%d.%m.%Y',
              'tr':'%d.%m.%Y','uk':'%d.%m.%Y','vi':'%d/%m/%Y',
              'zh-CN':'%y-%m-%d','zh-HK':'%d-%m-%Y','zh-TW':'%y/%m/%d'}

RE_MOBILE = re.compile(r"(iphone|ipod|blackberry|android|palm|windows\s+ce)", re.I)
RE_DESKTOP = re.compile(r"(windows|linux|os\s+[x9]|solaris|bsd)", re.I)
RE_BOT = re.compile(r"(spider|crawl|slurp|bot)", re.I)
mxdist = 80 # max distance km


def toint(s):
    try:
        t=int(s)
        return t
    except ValueError:
        return 0


class BaseHandler(tornado.web.RequestHandler):
    @property
    def db(self):
        return self.application.db

    @property
    def is_ajax(self):
        request_x = self.request.headers.get('X-Requested-With')
        return request_x == 'XMLHttpRequest'

    def get_current_user(self):
        return self.get_secure_cookie("user")

    def get_error_html(self, status_code, **kwargs):
        if status_code == 404:
            self.render('404.html',page_title="Error page")
        else:
            self.render('unknown.html',page_title="Unknown page")

    def get_locales(self):
       codes = ['en']
       if "Accept-Language" in self.request.headers:
            languages = self.request.headers["Accept-Language"].split(",")
            locales = []
            for language in languages:
                parts = language.strip().split(";")
                if len(parts) > 1 and parts[1].startswith("q="):
                    try:
                        score = float(parts[1][2:])
                    except (ValueError, TypeError):
                        score = 0.0
                else:
                    score = 1.0
                locales.append((parts[0], score))
            if locales:
                locales.sort(key=lambda (l, s): s, reverse=True)
                codes = [l[0] for l in locales]
       return codes

    def get_dateformat(self):
        lcl = self.get_locales()[0]
        dtf = dateformats.get(lcl,'%m/%d/%Y')
        return dtf

    def initialize(self, *args, **kwargs):
        self.remote_ip = self.request.headers.get('X-Forwarded-For', self.request.headers.get('X-Real-Ip', self.request.remote_ip))
        print("ip",self.remote_ip)
        self.using_ssl = (self.request.headers.get('X-Scheme', 'http') == 'https')
        user_agent = self.request.headers.get('User-Agent', '')
        self.desktop = not bool(RE_MOBILE.search(user_agent)) and \
                        bool(RE_DESKTOP.search(user_agent)) or \
                        bool(RE_BOT.search(user_agent))
        self.bot = bool(RE_BOT.search(user_agent))
        self.mobile = not self.desktop
        self.page_title = "Live music nearby,Find music concerts near me"
        self.message = ""
        self.description = "Find all music events in your city. If you get bored go to the nearest concert and feel joy from live music."
        print("user_agent", user_agent)

    @gen.coroutine
    def get_venues(self,lon, lat, mxdist=100):
        #Get list of nearest venues from the database. mxdist is a max distance km
        maxdist = mxdist / 6372.8
        if type(lon) is not float:
            lon = tofloat(lon)
        if type(lat) is not float:
            lat = tofloat(lat)

        res = []
        if lat != None and lon != None:
            y1 = lon - maxdist
            x1 = lat - maxdist
            y2 = lon + maxdist
            x2 = lat + maxdist
            qr = "select venue_guid,venue_name,longlat,website,bit_venue_id \
                                            from venues where longlat <@ box '((%s,%s),(%s,%s))'" % (y1, x1, y2, x2)
            cursor = yield self.db.execute("select venue_guid,venue_name,longlat,website,bit_venue_id \
                                            from venues where longlat <@ box '((%s,%s),(%s,%s))'", (y1, x1, y2, x2))
            rows = cursor.fetchall()
            res = []
            for r in rows:
                r["dist"] = geocalc(lon,lat,r["longlat"].x,r["longlat"].y)
                res.append(r)
            if len(res) > 1:
                res = sorted(res,key=lambda x: x["dist"])
       # print("get_venues", lon, lat,len(res),qr)
        raise gen.Return(res[0:100])

    @gen.coroutine
    def get_event(self,event_guid):
        cursor = yield self.db.execute("SELECT * FROM events WHERE event_guid=%s", (event_guid,))
        row = None
        ven = None
        rows = cursor.fetchall()
        if len(rows) > 0:
            row = rows[0]
            cursor = yield self.db.execute("SELECT * FROM venues WHERE venue_guid=%s", (row["venue_guid"],))
            vens = cursor.fetchall()
            if len(vens) > 0: ven=vens[0]
        raise gen.Return((row,ven))

    @gen.coroutine
    def get_nearest_events(self,lon, lat,cur_time, mxdist=100):
        #Get list of nearest events from the database. mxdist is a max distance km
        maxdist = mxdist / 6372.8
        if type(lon) is not float:
            lon = tofloat(lon)
        if type(lat) is not float:
            lat = tofloat(lat)
        cur_time = cur_time - datetime.timedelta(hours=3)
        res = []
        if lat != None and lon != None:
            y1 = lon - maxdist
            x1 = lat - maxdist
            y2 = lon + maxdist
            x2 = lat + maxdist
            qr = "select event_guid,event_time,venue_name,artist_name,title,country,region,city,longlat \
                      from events where longlat <@ box '((%s,%s),(%s,%s))' and event_time > %s"
            cursor = yield self.db.execute(qr, (y1, x1, y2, x2,cur_time))
            rows = cursor.fetchall()
            res = []
            for r in rows:
                r["dist"] = geocalc(lon,lat,r["longlat"].x,r["longlat"].y)
                res.append(r)
            if len(res) > 1:
                res = sorted(res,key=lambda x: (x["event_time"],x["dist"]))
       # print("get_venues", lon, lat,len(res),qr)
        raise gen.Return(res[0:100])


    @gen.coroutine
    def events_venue_bit(self,bit_url, startDate=datetime.datetime.now()):
        print(bit_url)
        # Get events for the venue from bandsintown

        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(bit_url)
        contents = response.body.decode('utf-8', 'ignore')
        lis = []
        if "<table" in contents:
            tree = html.fromstring(contents)
            lis = tree.xpath('//table//tr//@data-event-link')
            print(lis)
        raise gen.Return(lis)

class MainHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        chkd = ""
        dsbl = "disabled"
        sd = self.get_argument("dp1", "")
        city_id = self.get_argument("city_id", "")
        city = self.get_argument("city", "")
        startDate = self.get_argument("sd", "")
        lon = self.get_argument("lon", "")
        lat = self.get_argument("lat", "")
        lon2 = self.get_argument("lon2", "")
        lat2 = self.get_argument("lat2", "")
        entloc = self.get_argument("entloc", "")
        if entloc == "on":
            chkd = "checked"
            dsbl = ""

        print("loc coordinate get", lon2, lat2)
        dtf = self.get_dateformat()
        startDate = datetime.datetime.today()
        region = ""
        country = ""
        startDate = datetime.datetime.today()
        if sd != "":
            try:
                startDate = datetime.datetime.strptime(sd, dtf)
            except:
                startDate = datetime.datetime.today()
        sd = datetime.datetime.strftime(startDate, dtf)

        hdr1 = ""  #""Music near by"
        dtf = self.get_dateformat()
        user_ip =self.request.headers.get("X-Real-Ip","")
        mobile = not self.desktop # True for desktop and False for mobile
        bot = self.bot         # It's not user only bot
        if len(user_ip) < 7:
            user_ip = "91.238.23.45"

        if city == "":
            city, region, country, loc = yield city_by_ip(user_ip)
            crd = loc.split(",")
            if len(crd) == 2:
                lon = tofloat(crd[1])
                lat = tofloat(crd[0])
        print("city", city,lon,lat)
        d = ""
        if lon != "" and lat != "":
            rows = yield self.get_nearest_events(lon, lat, startDate, mxdist)

            d = '<table class="table"><thead><tr><th>Time</th><th> Event </th><th> distance,km </th></tr></thead>'
            for r in rows:
                title = r['artist_name'] + " - " + r['venue_name']

                d += '<tr><td>%s</td><td><a href="/event?id=%s">%s</a></td><td>%s</td></tr>' % (r["event_time"].strftime('%Y-%m-%d %H:%M'),
                                                                r["event_guid"], title, round(r["dist"], 3))
            d += '</table>'

        if region != "" : city += ", " + region
        if country != "": city += ", " + country
        self.render("index.html", page_title="Music near by",hdr1=hdr1,msg="",error="",getdata=d,lon=lon,lat=lat,city=city,
                    city_id=city_id,sd=sd,checked=chkd,disabled=dsbl,val_etnloc=entloc)

    @gen.coroutine
    def post(self):
        if self.is_ajax:
            city_id = self.get_argument("city_id", "")
            city = self.get_argument("city", "")
            startDate = self.get_argument("sd", "")
            lon= self.get_argument("lon", "")
            lat = self.get_argument("lat", "")
            lon2 = self.get_argument("lon2", "")
            lat2 = self.get_argument("lat2", "")
            entloc = self.get_argument("entloc", "")
            tz = toint(self.get_argument("tz", 0))
            server_tz = time.timezone / 60
            min_offset = server_tz - tz
            if min_offset < 0:
                user_time = datetime.datetime.now() - datetime.timedelta(minutes=-min_offset)
            else:
                user_time = datetime.datetime.now() + datetime.timedelta(minutes=min_offset)
           # print("user_time",user_time)
            if startDate != "":
                startDate = dparser.parse(startDate,fuzzy=True)
            else:
                startDate = datetime.datetime.today()
            addr = ""
            if entloc != "on":
                if lon2 != "" and lat2 != "":
                    addr = yield address_by_coordinates(lat2,lon2)
                    lon = lon2
                    lat = lat2
                    city = addr
            else:
                long, latg = yield google_location_async(city)
             #   print("google_location_async",city,long,latg)
                if long != 0 or latg != 0:
                    lon = long
                    lat = latg
            # if city_id==None or city_id=="":
            #     city_id = self.get_secure_cookie('city_ide')
            # else:
            #     self.set_secure_cookie('city_ide', city_id)
            if city==None or city=="":
                city = self.get_secure_cookie('city')
                # lon = self.get_secure_cookie('lon')
                # lat = self.get_secure_cookie('lat')
            else:
                self.set_secure_cookie('city', city)
                self.set_secure_cookie('lon', str(lon))
                self.set_secure_cookie('lat', str(lon))

            rows = []
            if lon != "" and lat != "":
                rows = yield self.get_nearest_events(lon, lat,startDate, mxdist)

            d = '<table class="table"><thead><tr><th>Time</th><th> Event </th><th> distance,km </th></tr></thead>'
            for r in rows:
                title = r['artist_name'] + " - " + r['venue_name']

                d += '<tr><td>%s</td><td><a href="/event?id=%s">%s</a></td><td>%s</td></tr>' % (r["event_time"].strftime('%Y-%m-%d %H:%M'),
                                                                r["event_guid"], title, round(r["dist"], 3))
            d += '</table>'
            data={"events": d,"addr": addr}
            data=json.dumps(data)
            self.write(data)
        self.finish()
def noempty(par):
    if par != None and par.strip() != "":
        return True
    return False

class EventHandler(BaseHandler):
    @gen.coroutine
    def get(self):
        event_guid = self.get_argument("id", "")
        title = "Music Event "
        hdr = ""
        data = ""
        evinfo,veninfo = yield self.get_event(event_guid)
        if evinfo:
            vnm = evinfo["venue_name"]
            lon = evinfo["longlat"].x
            lat = evinfo["longlat"].y
            city = evinfo["city"]
            data += "<h2>%s  -  %s</h2>" % (evinfo["event_time"].strftime("%b-%d %H:%M"),evinfo["artist_name"])
            data +="<h3> %s </h3>" % evinfo["venue_name"]
            data += "<address>"
            if noempty(evinfo["street"]): data += evinfo["street"] + "<br>"
            if noempty(evinfo["city"]): data +=  evinfo["city"] + "<br>"
            if noempty(evinfo["region"]) and not evinfo["region"].isdigit() : data +=  evinfo["region"] + "<br>"
            if noempty(evinfo["country"]): data += evinfo["country"] + "<br>"
            if veninfo != None:
                lon = veninfo["longlat"].x
                lat = veninfo["longlat"].y
                if noempty(veninfo["phone"]):
                    data += '<a href="tel:%s">%s</a>' % (veninfo["phone"],veninfo["phone"])
            data += "</address>"
            data +='''<div class="component venue-map">
		    <a href="http://maps.google.com/maps?ll={}%2C{}&q={}&z=15" target="_blank">
			<img src="http://maps.googleapis.com/maps/api/staticmap?maptype=roadmap&markers=color%3Ablue%7C{}%2C{}
			&sensor=false&size=355x257&zoom=13" alt="{}"> </a></div>'''.format(lat,lon, vnm+ ", "+city,lat,lon,vnm)
        self.render("event.html", page_title=title, hdr=hdr, data=data)

class VenuesHandler(BaseHandler):
    @tornado.web.authenticated
    @gen.coroutine
    def get(self):
        hdr1 = "Music near by-venues"
        self.render("index.html", page_title=hdr1,hdr1=hdr1,msg="",error="" )

class CityHandler(BaseHandler):
    #Autocomplete AJAX
    @tornado.web.asynchronous
    @gen.coroutine
    def post(self):
        if self.is_ajax:
            city = self.get_argument("term", "")

            cursor = yield self.db.execute("select * from cities where city like %s limit 10",
                                           (city+"%",))
            rows = cursor.fetchall()
            data=[]
            for row in rows:
                d={"label":row['city'] + ", " + row['country'],"value": row['city_guid'],"lon":row["longlat"].y,
                   "lat":row["longlat"].x}
                data.append(d)
            data=json.dumps(data)
            self.write(data)
        self.finish()

class AuthLoginHandler(BaseHandler):
    def get(self):
        try:
            errormessage = self.get_argument("error")
        except:
            errormessage = ""
        self.page_title = "Login "
        self.render("login.html", header_text="Login",error=errormessage)

    def check_permission(self, password, usr):
        if (usr.lower() == "serge" and password.lower() == "korsunenko") :
            return True
        return False

    def post(self):
        username = self.get_argument("usr", "")
        password = self.get_argument("password", "")
        auth = self.check_permission(password, username)
        if auth:
            self.set_current_user(username)
            self.redirect(prefix+'/')
            return
        else:
            error_msg = u"?error=" + tornado.escape.url_escape("Login incorrect")
            self.redirect(prefix+u"/login" + error_msg)

    def set_current_user(self, user):
        if user:
            self.set_secure_cookie("user", user)
        else:
            self.clear_cookie("user")

class AuthLogoutHandler(BaseHandler):
    def get(self):
        self.clear_cookie("user")
        self.redirect(self.get_argument("next", prefix+"/"))

class ErrorHandler(tornado.web.RequestHandler):
    def get(self,w):
        self.render('404.html',page_title="Error page")

class ActivateHandler(BaseHandler):
#    @tornado.web.authenticated
    @tornado.web.asynchronous
    @gen.coroutine
    def get(self,**params):
        innum = self.get_argument("innum", "")
        incod = toint(innum)
        actcode = "0"
        if incod > 0:
            actcode = str(incod * 12 +1959)
        self.write(actcode)
        self.finish()

class Check1Handler(BaseHandler):
    def get(self):
        data = "3ajmvgvDPiCf6eI0bzZYRkdddduVNIoGr-NnjhwTo1E.nqqrzS0pcTh5-Iz_TiHfInPuW2CTrom4yZmDcsdbmCQ"
        self.write(data)

class Check2Handler(BaseHandler):
    def get(self):
        data = "BoT5Gglj3J7dOyoG120G3d5B2b8ntrrg2SDyOiAD7Is.nqqrzS0pcTh5-Iz_TiHfInPuW2CTrom4yZmDcsdbmCQ"
        self.write(data)

class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (prefix + r"/", MainHandler),
            (prefix + r"/index", MainHandler),
            (prefix + r"/event", EventHandler),
            (prefix + r"/venues", VenuesHandler),
            (prefix + r"/artist", ActivateHandler),
            (prefix + r"/city", CityHandler),
            (prefix + r"/login", AuthLoginHandler),
            (prefix + r"/logout", AuthLogoutHandler),
            (prefix + r"/.well-known/acme-challenge/3ajmvgvDPiCf6eI0bzZYRkdddduVNIoGr-NnjhwTo1E", Check1Handler),
            (prefix + r"/.well-known/acme-challenge/BoT5Gglj3J7dOyoG120G3d5B2b8ntrrg2SDyOiAD7Is", Check2Handler),
            (prefix + r"/(.*)", ErrorHandler),
            (r"/(.*)", ErrorHandler)
            ]
        settings = {
            "template_path":TEMPLATE_PATH,
            "static_path":STATIC_PATH,
            "debug":DEBUG,
            "cookie_secret":COOKIE_SECRET,
            "xsrf_cookies": True,
             "login_url": prefix + "/login",
        }
        tornado.web.Application.__init__(self, handlers, **settings)



def time_str(t,only_time=False):
    try:
        if only_time:
            return datetime.datetime.utcfromtimestamp(int(t)).strftime('%H:%M:%S')
        return datetime.datetime.utcfromtimestamp(int(t)).strftime('%Y-%m-%d %H:%M:%S')
    except:
        return t

def str_to_timeUnix(st,incr=0):
    t = strptime(st,"%Y-%m-%d")
    dt =datetime.datetime(t.tm_year,t.tm_mon,t.tm_mday)
    if incr > 0:
        dt = dt + datetime.timedelta(days=incr)
    ts = (dt - datetime.datetime(1970,1,1)).total_seconds()
    return ts


if __name__ == '__main__':
    tornado.options.parse_command_line()
  #  sch = check_database()
    if exist_table('public','invusers') == False:
        create_table_invusers()
    if exist_table('public', 'usergroups') == False:
        create_table_usergroups()
    if exist_table('public', 'venues') == False:
        create_table_venues()
    if exist_table('public', 'cities') == False:
        create_table_cities()
    if exist_table('public', 'events') == False:
        create_table_events()
    if exist_table('public', 'artists') == False:
        create_table_artists()
    register_point()
    application = Application()
    ioloop = tornado.ioloop.IOLoop.instance()
    dsn = 'dbname=%s user=%s password=%s host=%s port=%s' % (DB_NAME, DB_USER, DB_PWD, DB_HOST, DB_PORT)
    application.db = momoko.Pool(dsn=dsn, size=5, cursor_factory=psycopg2.extras.RealDictCursor, ioloop=ioloop)
    # this is a one way to run ioloop in sync
    future = application.db.connect()
    ioloop.add_future(future, lambda f: ioloop.stop())
    ioloop.start()
    future.result()  # raises exception on connection error

    http_server = tornado.httpserver.HTTPServer(application, xheaders=True)
    http_server.listen(options.port)
    print('Listen port: %s' % options.port)
    try:
        ioloop.start().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()
        logging.info('exit success')

    # http_server = tornado.httpserver.HTTPServer(Application())
    # http_server.listen(options.port)
    # print('Listen port: %s' % options.port)
    # try:
    #     tornado.ioloop.IOLoop.instance().start()
    # except KeyboardInterrupt:
    #     tornado.ioloop.IOLoop.instance().stop()
    #     logging.info('exit success')