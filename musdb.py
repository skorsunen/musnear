import os,time,sys
if sys.version_info[0] < 3:
    reload(sys)
    sys.setdefaultencoding('utf-8')
import uuid
import psycopg2
import psycopg2.extras
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT,adapt, register_adapter, AsIs
from collections import OrderedDict
import re, random
DB_NAME = "musnear"
DB_USER = "serge"
DB_PWD =  "2222222"
DB_HOST = "localhost"
DB_PORT = "5432"
if os.name == "nt":
    DB_NAME = "musnear"
    DB_USER = "serge"
    DB_PWD = "222222"
    DB_HOST = "localhost"
    DB_PORT = "5432"

class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

def adapt_point(point):
    x = adapt(point.x).getquoted()
    y = adapt(point.y).getquoted()
    return AsIs("'(%s, %s)'" % (x, y))

def cast_point(value, cur):
    if value is None:
        return None
    # Convert from (f1, f2) syntax using a regular expression.
    m = re.match(r"\(([^)]+),([^)]+)\)", value)
    if m:
        return Point(float(m.group(1)), float(m.group(2)))
    else:
        raise psycopg2.InterfaceError("bad point representation: %r" % value)

def register_point():
    register_adapter(Point, adapt_point)
    con = get_db_connect()
    cur = con.cursor()
    cur.execute("SELECT NULL::point")
    point_oid = cur.description[0][1]
    POINT = psycopg2.extensions.new_type((point_oid,), "POINT", cast_point)
    psycopg2.extensions.register_type(POINT)


def get_db_connect():
    if os.name == "nt":
        return psycopg2.connect(database=DB_NAME, user=DB_USER,password=DB_PWD,host=DB_HOST,port=DB_PORT,cursor_factory = psycopg2.extras.RealDictCursor)
    else:
        return psycopg2.connect(database=DB_NAME,
                            cursor_factory=psycopg2.extras.RealDictCursor)


def get_default_connect():
    return psycopg2.connect(database='postgres', user=DB_USER,password=DB_PWD,host=DB_HOST,port=DB_PORT,cursor_factory = psycopg2.extras.RealDictCursor)


def create_update(sql,params=[]):
    #execute create,alter,insert or update queries
    con = None
    res = False
    try:
        con = get_db_connect()
        cur = con.cursor()
        if len(params) > 0:
            params = tuple(params)
            cur.execute(sql,params)
        else:
            cur.execute(sql)
        con.commit()
        res = True
    except psycopg2.DatabaseError as e:
        if con:
            con.rollback()
        print('Error %s' % e)
    finally:
        if con:
            con.close()
    return res

def select_query(sql,params = []):
    #Execute Select query
    con = None
    rows = []
    try:
        con = get_db_connect()
        cur = con.cursor()
        if len(params) > 0:
            params = tuple(params)
            cur.execute(sql,params)
        else:
            cur.execute(sql)
        rows = cur.fetchall()
    except psycopg2.DatabaseError as e:
        print('Error %s' % e)
    finally:
        if con:
           con.close()
    return rows

def exist_table(tableschema,tablename):
    #Check existed table
    con = None
    row = None
    try:
        con = get_db_connect()
        cur = con.cursor()
        cur.execute("SELECT table_name FROM information_schema.tables \
            WHERE table_schema = %s and table_name = %s", (tableschema,tablename))
        row = cur.fetchone()
    except psycopg2.DatabaseError as e:
        print('Error %s' % e)
    finally:
        if con:
           con.close()
    return row != None


def check_schema(schema):
    #check existed schema
    con = None
    row = None
    try:
        con = get_db_connect()
        cur = con.cursor()
        cur.execute("SELECT schema_name FROM information_schema.schemata \
       WHERE schema_name = '%s'" % schema)
        row = cur.fetchone()
    except psycopg2.DatabaseError as e:
        print('Error %s' % e)
    finally:
        if con:
           con.close()
    return row != None


def check_database(dbname=DB_NAME):
    try:
        con = get_db_connect()
    except Exception as ex:
        print(ex)
        con = get_default_connect()
        con.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        cur = con.cursor()
        cur.execute('CREATE DATABASE ' + dbname)
        cur.close()
        con.close()
        print(dbname,"is created")
    rows = all_schemas()
    return rows

def all_schemas():
    #get all existed schemas
    con = None
    rows = []
    try:
        con = get_db_connect()
        cur = con.cursor()
        cur.execute("SELECT schema_name FROM information_schema.schemata")
        rows = cur.fetchall()
    except psycopg2.DatabaseError as e:
        print('Error %s' % e)
    finally:
        if con:
           con.close()
    return rows

def all_tables(schema=""):
    #get all existed schemas
    con = None
    rows = []
    try:
        con = psycopg2.connect(database=DB_NAME, user=DB_USER,password=DB_PWD,host=DB_HOST,port=DB_PORT,cursor_factory = psycopg2.extras.RealDictCursor)
        cur = con.cursor()
        if schema == "":
            cur.execute("SELECT table_name,table_schema FROM information_schema.tables")
        else:
            cur.execute("SELECT table_name,table_schema FROM information_schema.tables \
                        WHERE table_schema = '%s' " % schema)

        rows = cur.fetchall()
    except psycopg2.DatabaseError as e:
        print('Error %s' % e)
    finally:
        if con:
           con.close()
    return rows

def check_field(table,field):
    fields = table_structure(table)
    for fld in fields:
        if fld['column_name'].lower() == field.lower():
            return True
    return False

def check_index(indx):
    sql = "SELECT count(*) as a FROM pg_class c  WHERE c.relname = %s AND c.relkind = 'i';"
    rows = select_query(sql,[indx])
    if len(rows) > 0 and rows[0].get('a',0) > 0:
        return True
    return False

def create_json_indexes():
    sql_ind = []
    if not check_index('index_event_fbid') :
        sql_ind.append("CREATE INDEX index_event_fbid ON events USING BTREE ((data->>'fb_event_id'));")
    if not check_index('index_ven_fbid') :
        sql_ind.append("CREATE INDEX index_ven_fbid ON events USING BTREE ((data->>'fb_ven_id'));")
    if not check_index('index_art_fbid') :
        sql_ind.append("CREATE INDEX index_art_fbid ON artists USING BTREE ((data->>'fb_art_id'));")
    if len(sql_ind) == 0: return
    con = None
    try:
        con = get_db_connect()
        cur = con.cursor()
        for sql in sql_ind:
            cur.execute(sql)
        con.commit()
        print("It was created the indexes")
    except psycopg2.DatabaseError as e:
        if con:
            con.rollback()
        print('Error %s' % e)
    finally:
        if con:
            con.close()


def table_structure(table,schema=""):
    #get structure of the table
    con = None
    rows = []
    try:
        con = psycopg2.connect(database=DB_NAME, user=DB_USER,password=DB_PWD,host=DB_HOST,port=DB_PORT,cursor_factory = psycopg2.extras.RealDictCursor)
        cur = con.cursor()
        if schema == "":
            cur.execute("SELECT table_schema,table_name,column_name,data_type,character_maximum_length,is_nullable,column_default FROM \
                         information_schema.columns WHERE table_name = '%s'" % table)
        else:
            cur.execute("SELECT table_name,table_schema FROM information_schema.tables \
                        WHERE table_schema = '%s' AND table_name = '%s' " % (schema,table))

        rows = cur.fetchall()
    except psycopg2.DatabaseError as e:
        print('Error %s' % e)
    finally:
        if con:
           con.close()
    return rows

def create_table_invusers():
    sql = """
        CREATE TABLE IF NOT EXISTS invusers (
            id bigserial PRIMARY KEY ,
            user_guid varchar(40) UNIQUE,
            username  varchar(40),
            email  varchar(100) ,
            password  varchar(100),
            company_guid  varchar(40),
            role  varchar(40),
            status varchar(20),
            usergroup varchar(40),
            comments  varchar(500) ,
            date_added timestamp default NULL
        );

    """
    sql_ind1 = "CREATE INDEX index_user_guid ON invusers (user_guid);"
    sql_ind2 = "CREATE INDEX index_company_guid_username ON invusers (company_guid,username);"

    con = None
    try:
        con = get_db_connect()
        cur = con.cursor()
        cur.execute(sql)
        cur.execute(sql_ind1)
        cur.execute(sql_ind2)
        con.commit()
        print("It was created the table invusers")
    except psycopg2.DatabaseError as e:
        if con:
            con.rollback()
        print('Error %s' % e)
    finally:
        if con:
            con.close()

def create_table_usergroups():
    sql = """
        CREATE TABLE IF NOT EXISTS usergroups (
            id bigserial PRIMARY KEY ,
            group_guid varchar(40) UNIQUE,
            groupname  varchar(40),
            comments  varchar(250) ,
            status varchar(20),
            date_added timestamp default NULL
        );
    """
    sql_ind1 = "CREATE INDEX index_group_guid ON usergroups (group_guid);"
    con = None
    try:
        con = get_db_connect()
        cur = con.cursor()
        cur.execute(sql)
        cur.execute(sql_ind1)
        con.commit()
        print("It was created the table usergroups")
    except psycopg2.DatabaseError as e:
        if con:
            con.rollback()
        print('Error %s' % e)
    finally:
        if con:
            con.close()

def create_table_venues():
    sql = """
        CREATE TABLE IF NOT EXISTS venues (
            id bigserial PRIMARY KEY ,
            venue_guid varchar(40) UNIQUE,
            category varchar(40),
            venue_name  varchar(200),
            website  varchar(200),
            email  varchar(200),
            country  varchar(100),
            region  varchar(50),
            city  varchar(100),
            street  varchar(200),
            postcode  varchar(50),
            phone  varchar(100),
            bit_venue_id  bigint,
            sk_venue_id  bigint,
            longlat point,
            links text,
            data jsonb,
            comments  text,
            status varchar(20),
            date_added timestamp default NULL
        );
    """
    sql_ind = []
    sql_ind.append("CREATE INDEX index_ven_guid ON venues (venue_guid);")
    sql_ind.append("CREATE INDEX index_ven_name ON venues (venue_name);")
    sql_ind.append("CREATE INDEX index_ven_country ON venues (country);")
    sql_ind.append("CREATE INDEX index_ven_region ON venues (region);")
    sql_ind.append("CREATE INDEX index_ven_city ON venues (city);")
    sql_ind.append("CREATE INDEX index_ven_street ON venues (street);")
    sql_ind.append("CREATE INDEX index_ven_bit_venue_id ON venues (bit_venue_id);")
    sql_ind.append("CREATE INDEX index_ven_sk_venue_id ON venues (sk_venue_id);")
    sql_ind.append("CREATE INDEX index_longlat ON venues USING GIST (longlat point_ops);")
    con = None
    try:
        con = get_db_connect()
        cur = con.cursor()
        cur.execute(sql)
        for sql in sql_ind:
            cur.execute(sql)
        con.commit()
        print("It was created the table venues")
    except psycopg2.DatabaseError as e:
        if con:
            con.rollback()
        print('Error %s' % e)
    finally:
        if con:
            con.close()


def create_table_cities():
    sql = """
        CREATE TABLE IF NOT EXISTS cities (
            id bigserial PRIMARY KEY ,
            city_guid varchar(40) UNIQUE,
            country  varchar(50),
            region  varchar(100),
            city  varchar(100),
            street  varchar(200),
            longlat point,
            date_added timestamp default NULL
        );
    """
    sql_ind = []
    sql_ind.append("CREATE INDEX index_city_guid ON cities (city_guid);")
    sql_ind.append("CREATE INDEX index_city_city ON cities (city);")
    sql_ind.append("CREATE INDEX index_city_country ON cities (country);")
    sql_ind.append("CREATE INDEX index_city_region ON cities (region);")
    sql_ind.append("CREATE INDEX index_city_street ON cities (street);")
    sql_ind.append("CREATE INDEX index_city_longlat ON cities USING GIST (longlat point_ops);")
    con = None
    try:
        con = get_db_connect()
        cur = con.cursor()
        cur.execute(sql)
        for sql in sql_ind:
            cur.execute(sql)
        con.commit()
        print("It was created the table cities")
    except psycopg2.DatabaseError as e:
        if con:
            con.rollback()
        print('Error %s' % e)
    finally:
        if con:
            con.close()

def create_table_artists():
    sql = """
        CREATE TABLE IF NOT EXISTS artists (
            id bigserial PRIMARY KEY ,
            artist_guid varchar(40) UNIQUE,
            genre varchar(100),
            artist_name  varchar(200),
            website  varchar(250),
            email  varchar(100),
            country  varchar(50),
            region  varchar(50),
            city  varchar(50),
            street  varchar(100),
            postcode  varchar(50),
            phone  varchar(50),
            bit_url  varchar(150),
            bit_artist_id  bigint,
            sk_artist_id  bigint,
            summaries text,
            data jsonb,
            status varchar(20),
            date_added timestamp default NULL
        );
    """
    sql_ind = []
    sql_ind.append("CREATE INDEX index_art_artist_guid  ON artists (artist_guid);")
    sql_ind.append("CREATE INDEX index_art_artist_name ON artists (artist_name);")
    sql_ind.append("CREATE INDEX index_art_sk_artist_id ON artists (sk_artist_id);")
    sql_ind.append("CREATE INDEX index_art_bit_artist_id ON artists (bit_artist_id);")
    sql_ind.append("CREATE INDEX index_art_country ON artists (country);")
    con = None
    try:
        con = get_db_connect()
        cur = con.cursor()
        cur.execute(sql)
        for sql in sql_ind:
            cur.execute(sql)
        con.commit()
        print("It was created the table artists")
    except psycopg2.DatabaseError as e:
        if con:
            con.rollback()
        print('Error %s' % e)
    finally:
        if con:
            con.close()

def create_table_events():
    sql = """
        CREATE TABLE IF NOT EXISTS events (
            id bigserial PRIMARY KEY ,
            event_guid varchar(40) UNIQUE,
            event_time timestamp,
            venue_guid varchar(40),
            venue_name  varchar(150),
            artist_guid  varchar(40),
            artist_name  varchar(200),
            bit_id  bigint,
            bit_venue_id  bigint,
            bit_artist_id  bigint,
            sk_id  bigint,
            sk_venue_id  bigint,
            sk_artist_id  bigint,
            title varchar(250),
            country  varchar(50),
            region  varchar(50),
            city  varchar(50),
            street varchar(150),
            longlat point,
            comments  text ,
            data jsonb,
            status varchar(20),
            date_added timestamp default NULL
        );
    """
    sql_ind = []
    sql_ind.append("CREATE INDEX index_ev_guid ON events (event_guid);")
    sql_ind.append("CREATE INDEX index_ev_venue_guid ON events (venue_guid);")
    sql_ind.append("CREATE INDEX index_ev_artist_guid ON events (artist_guid);")
    sql_ind.append("CREATE INDEX index_ev_longlat ON events USING GIST (longlat point_ops);")
    sql_ind.append("CREATE INDEX index_ev_bit_venue_id ON events (bit_venue_id);")
    sql_ind.append("CREATE INDEX index_ev_bit_artist_id ON events (bit_artist_id);")
    sql_ind.append("CREATE INDEX index_ev_sk_venue_id ON events (sk_venue_id);")
    sql_ind.append("CREATE INDEX index_ev_sk_artist_id ON events (sk_artist_id);")
    sql_ind.append("CREATE INDEX index_ev_artist_name ON events (artist_name);")
    sql_ind.append("CREATE INDEX index_ev_time ON events (event_time);")
    sql_ind.append("CREATE INDEX index_ev_country ON events (country);")
    sql_ind.append("CREATE INDEX index_ev_region ON events (region);")
    sql_ind.append("CREATE INDEX index_ev_city ON events (city);")
    sql_ind.append("CREATE INDEX index_ev_street ON events (street);")
    sql_ind.append("CREATE INDEX index_ev_title ON events (title);")
    con = None
    try:
        con = get_db_connect()
        cur = con.cursor()
        cur.execute(sql)
        for sql in sql_ind:
            cur.execute(sql)
        con.commit()
        print("It was created the table events")
    except psycopg2.DatabaseError as e:
        if con:
            con.rollback()
        print('Error %s' % e)
    finally:
        if con:
            con.close()

def load_cities(filename="cities.txt"):
    st = time.time()
    i =  0
    sql = "INSERT INTO cities (city_guid,country,city,longlat) VALUES "
    vl = ""
    for line in open(filename,"r"):
        i += 1
        if i > 1:
            r = line.strip().split("\t")
            city_guid = str(uuid.uuid1())
            if vl != "": vl += ","
            vl += "('%s','%s','%s','(%s,%s)')" % (city_guid,r[1].replace("'","''"),r[0].replace("'","''"),r[2],r[3])
        if i % 1000 == 0:
            create_update(sql + vl)

            print(i,time.time() - st)
            vl = ""
         #   break
    if vl != "":
        create_update(sql + vl)
        print(i,time.time() - st)

def load_venues(filename="venues.txt"):
    st = time.time()
    i =  0
    sql = "INSERT INTO venues (venue_guid,category,venue_name,website,email,country,region,city,street,\
    postcode,phone,bit_url,longlat) VALUES "
    vl = ""
    for line in open(filename,"r"):
        i += 1
        if i > 1 :
            r = line.strip().split("\t")
            if len(r) != 14 or r[10].strip() == "" or r[11].strip() == "": continue
            cat = r[0].replace("[u'","").replace("']","").replace("'","''")
            nm = r[1].replace("'","''").strip().decode('utf-8','ignore')[0:200]
            wb = r[2].replace("'","''").strip().decode('utf-8','ignore')[0:200]
            em = r[3].replace("'", "''").strip().decode('utf-8','ignore')[0:200]
            cnt = r[4].replace("'", "''").strip().decode('utf-8','ignore')[0:100]
            rg = r[5].replace("'", "''").strip().decode('utf-8','ignore')[0:500]
            ct = r[6].replace("'", "''").strip().decode('utf-8','ignore')[0:100]
            street = r[7].replace("'", "''").strip().decode('utf-8','ignore')[0:200]
            pc = r[8].replace("'", "''").strip().decode('utf-8','ignore')[0:50]
            ph = r[9].replace("'", "''").strip().decode('utf-8','ignore')[0:100]
            bu = r[12].replace("'", "''").strip().decode('utf-8','ignore')[0:150]
            if r[13] == "":
                venue_guid = str(uuid.uuid1())
            else:
                venue_guid = r[13]
            if vl != "": vl += ","
            vl += "('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','(%s,%s)')" % (venue_guid,cat,nm,wb,em,cnt,rg,ct,street,pc,ph,bu,r[10],r[11])
        if i % 1000 == 0:
            create_update(sql + vl)

            print(i,time.time() - st)
            vl = ""
            #return
    if vl != "":
        create_update(sql + vl)
        print(i,time.time() - st)

if __name__ == '__main__':

  #  print adapt_point(Point(1,2))
    register_point()

    load_cities()
    load_venues()
  #   res = select_query("SELECT center(box '((0,0),(3,4))');")
  #   print res[0]["center"].x,res[0]["center"].y
    rows = select_query("select * from venues where longlat <@ box '((12,54),(13,56))';")
    for r in rows:
        print r["longlat"].x,r["longlat"].y,r["email"]
