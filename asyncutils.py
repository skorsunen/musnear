#-------------------------------------------------------------------------------
# Name:        asyncutils.py
# Purpose:    Asynchronous utils for live music
#
# Author:      Serge
#
# Created:     20/10/2014
# Copyright:   (c) Sydney 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import re,datetime,time
import random
import json
import requests
import dateutil.parser as dparser
import tornado.web
from tornado import gen
from tornado.httpclient import AsyncHTTPClient
from bson.objectid import ObjectId
from PIL import Image
from PIL import ImageFile
#from bs4 import BeautifulSoup, SoupStrainer,Comment
#import bs4,html2text
from musutils import *
import logging


logger = logging.getLogger(__name__)
handler = logging.FileHandler('livemusicutils.log')
handler.setLevel(logging.INFO)
### create a logging format
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
### add the handlers to the logger
logger.addHandler(handler)
headers = {'User-agent':'Mozilla/5.0'}
user_agents = [
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.107 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.102 Safari/537.36',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:26.0) Gecko/20100101 Firefox/26.0',
        'Mozilla/5.0 (Windows NT 6.1; rv:26.0) Gecko/20100101 Firefox/26.0',
        'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36'
    ]
cache_coord_address = {}
cache_address_coord = {}
@gen.coroutine
def load_facebook(db,info, artist_id):
    #Load artist information from Facebook into database
    ct = yield db.artists.find_one({'_id': ObjectId(artist_id)})
    logger.info("load_facebook start %s",artist_id )
    imgfolder = "static/img/artists/"
    summaries = ct.get('summaries',{})
    i = 0
    if ct != None:
        if  info.get('bio','') !='':
            summaries['facebook'] =  info['bio']
            ct['summaries'] = summaries
            i = 1
        if info.get('imglink','') !='':
            onlinefile = info['imglink']
            original_fname = onlinefile.split("/")[-1]
            extension = os.path.splitext(original_fname)[1][0:4]
            img_medium = artist_id + "_medium" + extension
            if saveimage(onlinefile, imgfolder + img_medium):
                ct['img_medium'] = img_medium
                i = 1
                im = Image.open(imgfolder + img_medium )
                im.thumbnail((128,128),Image.ANTIALIAS)
                im.save(imgfolder + artist_id +"_small.jpg", "JPEG")
                ct['img_small'] = artist_id + "_small.jpg"
        if ct.get('website','')=='' and info.get('website','')!='':
            ct['website'] = info['website']
        genre = info.get('genre','')
        if genre != '' :
            if ct.get('genre','')=='':
                ct['genre'] = genre
            elif genre not in ct['genre']:
                ct['genre'] += "," + genre
            i = 1
        fblink = info.get('fblink','')
        if fblink != '':
            ct['fblink'] = fblink
            i = 1
        areaname = info.get('areaname','')
        if areaname != '':
            ct['areaname'] = areaname
            i = 1
        if i > 0:
            res = yield db.artists.update({'_id':ct['_id']},ct)
            logger.info("load_facebook update %s",ct.get('name','') )


def eventlist(db,city_id,city,startDate,endDate,np=5,p=1):
    #Get venues near a city
    if city=="":
        longitude,latitude = city_point(db,city_id)
    else:
        longitude,latitude = city_location(db,city)
#    print "city",longitude,latitude
    if p < 1: p=1
    shift = (p-1) * np
    lim = shift + np + 1
    n = np
    if startDate > endDate:
        ss = endDate
        endDate = startDate
        startDate = endDate
    if startDate==endDate:
        endDate += datetime.timedelta(days=1)
#    rows = db.events.find({"loc": {"$near": [longitude, latitude]}}).skip(shift).limit(lim)
    maxdist = 100 / 6372.8   #   max distance 100 km
    res = db.events.aggregate([{
        '$geoNear': {
            'near': [longitude,latitude],
            'spherical': True,
            'distanceField': 'dist',
             'distanceMultiplier': 6372.8,   # for milles 3959,
             'maxDistance': maxdist,
             'query': {'eventtime':{'$gt':startDate,'$lt':endDate}} ,
            'num': lim}
    }, {
        '$skip': shift
    }])   #,{'$sort':{'eventtime': 1}}
    rows =res['result']
#    print "lim", lim,shift
    d = '<table class="table"><thead><tr><th>Time</th><th>Event</th><th>Distance</th></tr></thead><tbody>'
    p += 1
    i = 0
    for i,row in enumerate(rows):
        #don't show the last row
        if i < np:
#            distance ='      %.3f km' % geocalc(longitude,latitude,row['loc'][0],row['loc'][1])
            distance = '      %.3f km' % row['dist']
            id = str(row["_id"])
            d +='<tr><td>'+ row['eventtime'].strftime("%b-%d %H:%M") + '</td><td><a href="event?id='+ id+'">'+ row['artname'] +'<br>'+row['venuename']+ '</a>' +', '+row.get('city','')+'</td><td>'+distance + '</td></tr>'
        i += 1
    d += '</tbody></table>'
#    print "i",i
    nxt=prev = 0
    if i >= n :
        nxt = 1
    if shift > 0:
        prev = 1
    return d,nxt,prev


@gen.coroutine
def eventlist_async(db,city_id,city,startDate,endDate,np=5,p=1,mxdist=100,longitude=0,latitude=0):
    #Get venues near a city
    if longitude==0 and latitude==0:
        if city=="":
            longitude,latitude = yield city_point_async(db,city_id)
        else:
            longitude,latitude = yield city_location_async(db,city)
    if p < 1: p=1
    shift = (p-1) * np
    lim = shift + np + 1
    n = np
    if startDate > endDate:
        ss = endDate
        endDate = startDate
        startDate = endDate
    if startDate==endDate:
        endDate += datetime.timedelta(days=1)
    startDate = startDate.replace(hour=0)   #set time for begin of day
    endDate = endDate.replace(hour=23)     #set time to end of day
#    print  "eventlist_async",longitude,latitude,startDate,endDate,mxdist,type(startDate),shift,lim
    maxdist = mxdist / 6372.8   #   max distance 100 km
    pipeline = [{
        '$geoNear': {
            'near': [longitude,latitude],
            'spherical': True,
            'distanceField': 'dist',
             'distanceMultiplier': 6372.8,   # for milles 3959,
             'maxDistance': maxdist,
             'query': {'eventtime':{'$gt':startDate,'$lt':endDate}} ,
            'num': lim}
    }, {
        '$skip': shift
    }]

    d = '<table class="table"><thead><tr><th>Time</th><th>Event</th><th>Distance</th></tr></thead><tbody>'
    p += 1
    cursor =yield db.events.aggregate(pipeline,cursor={})
    i = 0
    artname1=""
    while (yield cursor.fetch_next):
        row = cursor.next_object()
        #don't show the last row
        if i < np:
            distance = '      %.3f km' % row['dist']
            id = str(row["_id"])
            if len(row['artists'])==1:
                artname= row['artists'][0]['name']
            else:
                artname= row['artists'][0]['name']+",..."
            if i==0:
                artname1 = row['artists'][0]['name']
            d +='<tr><td>'+ row['eventtime'].strftime("%b-%d %H:%M") + '</td><td><a href="/event?id='+ id+'">'+ artname +'<br>'+row['venuename']+ '</a>' +', '+row.get('city','')+'</td><td>'+distance + '</td></tr>'
        i += 1
    d += '</tbody></table>'
    logging.info("Length of events %s",len(d))
    nxt=prev = 0
    if i >= n :
        nxt = 1
    if shift > 0:
        prev = 1
    raise gen.Return((d,nxt,prev,longitude,latitude,artname1))


def venuelist(db,city_id,city,np=5,p=1):
    #Get venues near a city
    if city=="":
        longitude,latitude = city_point(db,city_id)
    else:
        longitude,latitude = city_location(db,city)
    if p < 1: p=1
    shift = (p-1) * np
    lim = shift + np + 1
    n = np
    maxdist = 100 / 6372.8   #   max distance 100 km
    res = db.venues.aggregate([{
        '$geoNear': {
            'near': [longitude,latitude],
            'spherical': True,
            'distanceField': 'dist',
             'distanceMultiplier': 6372.8,   # for milles 3959,
            'maxDistance': maxdist,
            'num': lim}
    }, {
        '$skip': shift
    }])
    rows =res['result']
    d = '<table class="table"><thead><tr><th>Name</th><th>City</th><th>Distance</th></tr></thead><tbody>'
    p += 1
    i = -1
    for i,row in enumerate(rows):
        #don't show the last row
        if i < n:
#            distance ='      %.3f km' % geocalc(longitude,latitude,row['loc'][0],row['loc'][1])
            distance = '      %.3f km' % row['dist']
            id = str(row["_id"])
            d +='<tr><td><a href="venue?id='+ id+'">'+ row.get("name",'') + '</a>' +'</td><td>'+ row.get("city",'')+'</td><td>' + distance + '</td></tr>'
    d += '</tbody></table>'
#    print "i",i
    nxt=prev = 0
    if i >= n:
        nxt = 1
    if shift > 0:
        prev = 1
#    print "nxt",nxt,"prev",prev
    return d,nxt,prev

def artlist(db,artsearch,np=5,p=1):
    #Get venues near a city
    if p < 1: p=1
    shift = (p-1) * np
    lim = np+1
    n = np
    if artsearch==None or artsearch=="": artsearch="a"
    rows = db.artists.find({"name": {'$regex': artsearch, '$options': 'i'}}).sort("name",1).skip(shift).limit(lim)
    d = '<table class="table"><thead><tr><th>Name</th><th>Image</th></tr></thead><tbody>'
    p += 1
    i = 0
    for i,row in enumerate(rows):
        #don't show the last row
        if i < n:
            id = str(row["_id"])
            d +='<tr><td><a href="artist?id='+ id+'">'+ row.get("name",'') + '</a>'\
             +'</td><td><span class="span1"><a href="artist?id='+ id+'">\
             <img src="static/img/artists/'+id+'_small.jpg' +'"></a></span></td></tr>'
    d += '</tbody></table>'
#    print "i",i
    nxt=prev = 0
    if i >= n:
        nxt = 1
    if shift > 0:
        prev = 1
#    print "nxt",nxt,"prev",prev
    return d,nxt,prev


@gen.coroutine
def venuelist_async(db,city_id,city,np=5,p=1,mxdist=100):
    #Get venues near a city
    if city=="":
        longitude,latitude =yield city_point_async(db,city_id)
    else:
        longitude,latitude =yield city_location_async(db,city)
    if p < 1: p=1
    shift = (p-1) * np
    lim = shift + np + 1
    n = np
    maxdist = mxdist / 6372.8   #   max distance 100 km
    pipeline = [{
        '$geoNear': {
            'near': [longitude,latitude],
            'spherical': True,
            'distanceField': 'dist',
             'distanceMultiplier': 6372.8,   # for milles 3959,
            'maxDistance': maxdist,
            'num': lim}
    }, {
        '$skip': shift
    }]
    d = '<table class="table"><thead><tr><th>Name</th><th>City</th><th>Distance</th></tr></thead><tbody>'
    p += 1
    cursor =yield db.venues.aggregate(pipeline,cursor={})
    i = 0
    while (yield cursor.fetch_next):
        row = cursor.next_object()
        #don't show the last row
        if i < np:
            distance = '      %.3f km' % row['dist']
            id = str(row["_id"])
            d +='<tr><td><a href="/venue?id='+ id+'">' + row.get("name",'') +\
             '</a></td><td>' + row.get("city",'') + '</td><td>' + distance + '</td></tr>'
        i += 1
    d += '</tbody></table>'
    nxt=prev = 0
    if i >= n:
        nxt = 1
    if shift > 0:
        prev = 1
    raise gen.Return((d,nxt,prev))


@gen.coroutine
def artlist_async(db,artsearch,np=5,p=1):
    #Get venues near a city
    if p < 1: p=1
    shift = (p-1) * np
    lim = np+1
    n = np
    if artsearch==None or artsearch=="": artsearch="a"
    cursor = db.artists.find({"name": {'$regex': artsearch, '$options': 'i'}}).sort("name",1).skip(shift).limit(lim)
    d = '<table class="table"><thead><tr><th>Name</th><th>Image</th></tr></thead><tbody>'
    p += 1
    i = 0
#    for i,row in enumerate(rows):
    while (yield cursor.fetch_next):
        row = cursor.next_object()
        #don't show the last row
        if i < n:
            id = str(row["_id"])
            imsm=row.get('img_small','blank_art.jpg')
            d +='<tr><td><a href="artist?id='+ id+'">'+ row.get("name",'') + '</a>'\
             +'</td><td><span class="span1"><a href="/artist?id='+ id+'">\
             <img  class="artsmall" src="static/img/artists/'+ imsm +'"></a></span></td></tr>'
        i += 1
    d += '</tbody></table>'
    nxt=prev = 0
    if i >= n:
        nxt = 1
    if shift > 0:
        prev = 1
    raise gen.Return((d,nxt,prev))


@gen.coroutine
def events_venue_async(db,venue_id,np=5,p=1,startDate=datetime.datetime.now()):
    #Get events for the venue
#    print "events_venue_async",venue_id,type(venue_id)
    if p < 1: p=1
    shift = (p-1) * np
    lim = np+1
    n = np
    startDate = startDate.replace(hour=0)   #set time for begin of day
    cursor = db.events.find({'venue_id':venue_id,'eventtime':{'$gt':startDate}}).sort('eventtime', 1).skip(shift).limit(lim)
    d = '<table class="table"><thead><tr><th>Time</th><th>Event</th></tr></thead><tbody>'
    p += 1
    i = 0
    while (yield cursor.fetch_next):
        row = cursor.next_object()
        #don't show the last row
        if i < np:

            id = str(row["_id"])
            if len(row['artists'])==1:
                artname= row['artists'][0]['name']
            else:
                artname= row['artists'][0]['name']+",..."
            d +='<tr><td>'+ row['eventtime'].strftime("%b-%d %H:%M") + '</td><td><a href="/event?id='+ id+'">'+ artname + '</a>' + '</td></tr>'
        i += 1
    d += '</tbody></table>'
    logging.info("Length of venue events %s",len(d))
    nxt=prev = 0
    if i >= n :
        nxt = 1
    if shift > 0:
        prev = 1
    raise gen.Return((d,nxt,prev))


@gen.coroutine
def events_artist_async(db,artist_id,np=5,p=1,startDate=datetime.datetime.now()):
    #Get events for the venue
    if p < 1: p=1
    shift = (p-1) * np
    lim = np+1
    n = np
    startDate = startDate.replace(hour=0)   #set time for begin of day
#    print "events_artist_async",artist_id,startDate
    cursor = db.events.find({'artists.artist_id':artist_id,'eventtime':{'$gt':startDate}}).sort('eventtime', 1).skip(shift).limit(lim)
    d = '<table class="table"><thead><tr><th>Time</th><th>Event</th></tr></thead><tbody>'
    p += 1
    i = 0
    while (yield cursor.fetch_next):
        row = cursor.next_object()
        #don't show the last row
        if i < np:
            id = str(row["_id"])
            d +='<tr><td>'+ row['eventtime'].strftime("%b-%d %H:%M") + '</td><td><a href="/event?id='+ id+'">'+ row['venuename']+ '</a>' + '</td></tr>'
        i += 1
    d += '</tbody></table>'
    logging.info("Length of artist's events %s",len(d))
    nxt=prev = 0
    if i >= n :
        nxt = 1
    if shift > 0:
        prev = 1
    raise gen.Return((d,nxt,prev))


@gen.coroutine
def city_point_async(db,city_id):
    longitude = 0
    latitude = 0
    if city_id=="":
        raise gen.Return((longitude,latitude))
    try:
        ct =yield db.cities.find_one({'_id': ObjectId(city_id)})

        if ct!=None and type(ct) is dict:
            longitude = ct['loc'][0]
            latitude = ct['loc'][1]
    except Exception as ex:
        print ex
    raise gen.Return((longitude,latitude))





@gen.coroutine
def city_name_async(db,city_id):
    name=""
    if city_id=="":
        raise gen.Return(name)
    try:
        ct =yield db.cities.find_one({'_id': ObjectId(city_id)})
        if ct != None and type(ct) is dict:
            name = ct['city'] +', '+ct['country']
    except Exception as ex:
        print ex
    raise gen.Return(name)


@gen.coroutine
def city_location_async(db,address):
    longitude = 0
    latitude = 0
    if address==None: address='Cologne,DE'
    cc= address.split(',')
    if len(cc) == 2 and len(cc[1].strip()) == 2:
        city = cc[0].strip()
        country = cc[1].strip().upper()
        rcity="^"+city+"$"
        try:
            ct =yield db.cities.find_one({'city': {'$regex': rcity, '$options': 'i'},'country': country})
            if ct!=None and type(ct) is dict:
                longitude = ct['loc'][0]
                latitude = ct['loc'][1]
        except Exception as ex:
            print ex
    if longitude==0 and latitude==0:
#        print "before google",address
        longitude,latitude =yield google_location_async(address)
#        print "after google",longitude,latitude
    raise gen.Return((longitude,latitude))

@gen.coroutine
def google_location_async(address):
    longitude = latitude = 0
    try:
        ad = address.replace(',',' ')
        ad = ad.split()
        adqr = '+'.join(ad)
        if adqr in cache_address_coord:
            longitude, latitude = cache_address_coord[adqr]
        else:
            query="http://maps.googleapis.com/maps/api/geocode/json?address=" + adqr + "&sensor=false"
            http_client = AsyncHTTPClient()
            response = yield http_client.fetch(query)
            txt=response.body
          #  print(txt)
            geoinfo = json.loads(txt)
            res=geoinfo['results']
            if len(res) > 0 and 'geometry' in res[0] and 'location' in res[0]['geometry'] :
                loc=res[0]['geometry']['location']
                longitude,latitude = loc['lng'],loc['lat']
                print(res[0].get("formatted_address","---"))
                if longitude != 0 or latitude != 0:
                    cache_address_coord[adqr] = (longitude,latitude)
    except Exception as ex:
        print ex
    raise gen.Return((longitude,latitude))

@gen.coroutine
def get_bandsintown_events(db,location,startDate,endDate,pg=1):
    #Get information about a music events by location from bandsintown
    try:
        longitude,latitude = yield city_location_async(db,location)
        location = str(latitude) +','+str(longitude)
#        print "location",location
        sd = startDate.strftime("%Y-%m-%d")
        ed = endDate.strftime("%Y-%m-%d")
        urlroot = "http://www.bandsintown.com"
        urlb = "http://api.bandsintown.com/events/search.json?location=%s&date=%s,%s&page=%s&per_page=20&app_id=Live_Music_Near" % (location,sd,ed,pg)
#        print "get_bandsintown_events: url",  urlb
        nn = 0
        allatrists=[]
        #check repeated query
        ct = yield db.httpqueries.find_one({'url':urlb})
        if ct!=None and type(ct) is dict:
#            print "repeated query: url",  urlb
            raise gen.Return((nn,allatrists))

        #query to bandsintown API
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(urlb)
    #    logger.info(response)
    #    print "body", txt
    #    print "response",response
        if response != None and response.error == None:
            txt=response.body
            vq = 0
            eventlst = json.loads(txt)
            #save query in log table 'httpqueries'
            q={'url':urlb, "dateedit": datetime.datetime.utcnow(),"result":nn}
            res = yield db.httpqueries.insert(q)
            for event in eventlst:
                nn+=1
#                print event['datetime'], event['venue']['name']
                venue_id,venuename,street,city,region,country,phone,latitude,longitude =yield update_venue(event['venue'],db)
#                print venuename,latitude,longitude
                anm=[]
                artists=[]
                for art in event['artists']:
                    artnm,artist_id,bit_url =yield update_artist(art,db)
                    anm.append(artnm)
                    artists.append({"artist_id":artist_id,"name":artnm})
                    allatrists.append((artist_id,bit_url))
                artname=",".join(anm)
                url = event['url']
                f = url.find('?')
                if f > 7 :
                    url = url[:f]
                ticketurl = event['ticket_url']
    #            dt = dparser.parse(eventlst[0]['datetime'],fuzzy=True)
                dt = dparser.parse(event['datetime'],fuzzy=True)
                f = ticketurl.find('?')
                if f > 7 :
                    ticketurl = ticketurl[:f]
                bit_id = event['id']
    #            print "bit_id",bit_id
                vn = yield db.events.find_one({"bit_id": bit_id})
                p ={"loc":{ 'type': "Point", 'coordinates':[ tofloat(longitude), tofloat(latitude)]},"artname": artname, "eventtime": dt,"venuename": venuename,"street":street,
                "city": city,"country": country,"region":region,"venue_id":venue_id,"artists":artists,
                "bit_id":event['id'],"bit_url":url,"bit_ticket_url": ticketurl,"ticket_status":event['ticket_status'], "dateedit": datetime.datetime.utcnow()}
                if vn !=None:
                    p['_id']=vn['_id']
                res =yield db.events.save(p)
#            print nn
    except Exception as ex:
        print ex
    raise gen.Return((nn,allatrists))


@gen.coroutine
def load_events(db,eventlst):
    nn = 0
    allatrists=[]
    for event in eventlst:
        nn+=1
#        print event['datetime'], event['venue']['name']
        venue_id,venuename,street,city,region,country,phone,latitude,longitude =yield update_venue(event['venue'],db)
#            print venuename,latitude,longitude
        anm=[]
        artists=[]
        for art in event['artists']:
            artnm,artist_id,bit_url =yield update_artist(art,db)
            anm.append(artnm)
            artists.append({"artist_id":artist_id,"name":artnm})
            allatrists.append((artist_id,bit_url))
        artname=",".join(anm)
        url = event['url']
        f = url.find('?')
        if f > 7 :
            url = url[:f]
        ticketurl = event['ticket_url']
        dt = dparser.parse(event['datetime'],fuzzy=True)
        f = ticketurl.find('?')
        if f > 7 :
            ticketurl = ticketurl[:f]
        bit_id = event['id']
        vn = yield db.events.find_one({"bit_id": bit_id})
        p ={"loc":{ 'type': "Point", 'coordinates':[ tofloat(longitude), tofloat(latitude)]},"artname": artname, "eventtime": dt,"venuename": venuename,"street":street,
        "city": city,"country": country,"region":region,"venue_id":venue_id,"artists":artists,
        "bit_id":event['id'],"bit_url":url,"bit_ticket_url": ticketurl,"ticket_status":event['ticket_status'], "dateedit": datetime.datetime.utcnow()}
        if vn !=None:
            p['_id']=vn['_id']
        res =yield db.events.save(p)
#    print nn
    raise gen.Return((nn,allatrists))


@gen.coroutine
def update_venue(venue,db):
    #update venue from bandsintown.com
    name = venue['name']
    city = venue['city']
    bit_id = venue['id']  # bandsintown id
    country = venue['country']
    bit_url = venue['url']
    latitude = venue['latitude']
    longitude = venue['longitude']
    street,city,region,country,phone =yield get_venue_address(bit_url)
    vn =yield  db.events.find_one({"bit_id": bit_id})
    if vn == None:
        vn =yield  db.venues.find_one({"name": name,"city": city})
        p ={"loc":{ 'type': "Point", 'coordinates': [ tofloat(longitude), tofloat(latitude)]},"name": name,"street": street,
        "city": city,"region": region,"country": country,"phone": phone,'category':['Live Music Venue'],"bit_url":bit_url,"bit_id":bit_id}
        if vn == None:
            id =yield db.venues.insert(p)
 #           print name,id,"inserted"
        else:
            p["bit_url"]=bit_url
            p['bit_id'] = bit_id
            if street != "": p['street'] = street
            if city != "": p['city'] = city
            if region != "": p['region'] = region
            if country != "": p['country'] = country
            if phone != "": p['phone'] = phone
            res = yield db.venues.update({"name": name,"city": city},p)
            id = vn['_id']
#            print name,id,"updated"
    else:
        p ={'_id': ObjectId(vn['_id']),"loc":{ 'type': "Point", 'coordinates':[ tofloat(longitude), tofloat(latitude)]},"name": name,
        "street": street,"city": city,"region": region,"country": country,"phone": phone,"bit_url":bit_url,"bit_id":bit_id}
        id = yield db.venues.save(p)
        id = p['_id']
#        print name,id,"saved"
    raise gen.Return((id,name,street,city,region,country,phone,latitude,longitude))


@gen.coroutine
def get_venue_address(venue_url):
    #Get venue address from  bandsintown.com
    http_client = AsyncHTTPClient()
    response = yield http_client.fetch(venue_url)
    street = ""
    city = ""
    region = ""
    country = ""
    phone = ""
    latitude = 0
    longitude = 0
    try:
        txt=response.body
        soup = BeautifulSoup(txt)
        scc= gettxt(soup.find('span',{"class":"venue-location"}))
        scc = scc.split(',')
        street =scc[0]
        if len(scc) > 1:
            city = scc[1]
        if len(scc) > 2:
            country = scc[2].strip()
            if len(country)==2:
                region = country
                country = "USA"
        phone = gettxt(soup.find('span',{"class":"venue-phone"}))
        location =soup.find('img',src=re.compile("googleapis")).get('src','')
        location=urllib.unquote(location).decode('utf8')
    except Exception as ex:
        logger.error('get_venue_address Exception %s',ex)
##        print location
##        n = location.find("center=") + 7
##        k = location.find("&")
##        loc = location[n:k].split(',')
    raise gen.Return((street,city,region,country,phone))


@gen.coroutine
def get_venue_events(db,venue):
    #Get upcomming events for venue from  bandsintown.com
    nn = 0
    allatrists=[]
    try:
#        print "get_venue_events",venue['bit_id']
        query="http://api.bandsintown.com/venues/%s/events.json?app_id=Live_Music_Near" % venue['bit_id']
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(query)
        if response != None and response.error == None:
            txt=response.body
            vq = 0
            eventlst = json.loads(txt)
            if eventlst != None:
                for event in eventlst:
                    nn+=1
 #                   print event['datetime'], event['venue']['name']
            #        venue_id,venuename,street,city,region,country,phone,latitude,longitude =yield update_venue(event['venue'],db)
            #            print venuename,latitude,longitude
                    anm=[]
                    artists=[]
                    for art in event['artists']:
                        artnm,artist_id,bit_url =yield update_artist(art,db)
                        anm.append(artnm)
                        artists.append({"artist_id":artist_id,"name":artnm})
                        allatrists.append((artist_id,bit_url))
                    artname=",".join(anm)
                    url = event['url']
                    f = url.find('?')
                    if f > 7 :
                        url = url[:f]
                    ticketurl = event['ticket_url']
                    dt = dparser.parse(event['datetime'],fuzzy=True)
                    f = ticketurl.find('?')
                    if f > 7 :
                        ticketurl = ticketurl[:f]
                    bit_id = event['id']
                    vn = yield db.events.find_one({"bit_id": bit_id})
                    p ={"loc":venue["loc"],"artname": artname, "eventtime": dt,"venuename": venue['name'],"street":venue.get('street',''),
                    "city": venue.get('city',''),"country": venue.get('country',''),"region":venue.get('region',''),"venue_id":ObjectId(venue['_id']),"artists":artists,
                    "bit_id":event['id'],"bit_url":url,"bit_ticket_url": ticketurl,"ticket_status":event['ticket_status'], "dateedit": datetime.datetime.utcnow()}
                    if vn !=None:
                        p['_id']=vn['_id']
                    res =yield db.events.save(p)
#        print "get_venue_events",nn
    except Exception as ex:
        print ex
#    raise gen.Return((nn,allatrists))


@gen.coroutine
def get_artist_events(db,bit_url):
    #Get upcomming events for artist from  bandsintown.com
    nn = 0
    allatrists=[]
    bit_id = bit_url.split("/")[-1]
    query="http://api.bandsintown.com/artists/%s/events?format=json&app_id=Live_Music_Near" % bit_id
    http_client = AsyncHTTPClient()
    response = yield http_client.fetch(query)
    if response != None and response.error == None:
        txt=response.body
        vq = 0
        eventlst = json.loads(txt)
        if eventlst != None:
            for event in eventlst:
                nn+=1
#                print event['datetime'], event['venue']['name']
                venue_id,venuename,street,city,region,country,phone,latitude,longitude =yield update_venue(event['venue'],db)
        #            print venuename,latitude,longitude
                anm=[]
                artists=[]
                for art in event['artists']:
                    artnm,artist_id,bit_url =yield update_artist(art,db)
                    anm.append(artnm)
                    artists.append({"artist_id":artist_id,"name":artnm})
#                    allatrists.append((artist_id,bit_url))
                artname=",".join(anm)
                url = event['url']
                f = url.find('?')
                if f > 7 :
                    url = url[:f]
                ticketurl = event['ticket_url']
                dt = dparser.parse(event['datetime'],fuzzy=True)
                f = ticketurl.find('?')
                if f > 7 :
                    ticketurl = ticketurl[:f]
                bit_id = event['id']
                vn = yield db.events.find_one({"bit_id": bit_id})
                p ={"loc":{ 'type': "Point", 'coordinates':[ tofloat(longitude), tofloat(latitude)]},"artname": artname, "eventtime": dt,"venuename": venuename,"street":street,
                "city": city,"country": country,"region":region,"venue_id":venue_id,"artists":artists,
                "bit_id":event['id'],"bit_url":url,"bit_ticket_url": ticketurl,"ticket_status":event['ticket_status'], "dateedit": datetime.datetime.utcnow()}
                if vn !=None:
                    p['_id']=vn['_id']
                res =yield db.events.save(p)
 #           print nn
#    raise gen.Return((nn,allatrists))


@gen.coroutine
def update_artist(artist,db):
    #update artist from bandsintown.com
    name = artist['name']
    bit_url = artist['url']
    mbid = artist['mbid']
    vn =yield  db.artists.find_one({"bit_url": bit_url})
    if vn == None:
        vn =yield db.artists.find_one({"name": name})
        p ={"name": name,"mbid": mbid,"bit_url":bit_url}
        if vn == None:
            artist_id =yield db.artists.insert(p)
#            logging.info(name + " art inserted")
        else:
            p ={"bit_url":bit_url,"mbid": mbid,"name": name}
            res =yield db.artists.update({"name": name},p)
            artist_id = vn['_id']
#            logging.info(name +" art updated")
    else:
##        p ={'_id':vn['_id'],"name": name,"mbid": mbid,"bit_url":bit_url}
##        res = yield db.artists.save(p)
        artist_id = vn['_id']
#        logging.info(name+ " art existed")
    #yield save_artist_img(db,bit_url,artist_id)
    raise gen.Return((name,artist_id,bit_url))


@gen.coroutine
def get_artlinks(db,mbid):
    #Get information about artist by mbid from musicbrainz.org
    links=[]
    summaries={}
    country = ''
    areaname = ''
    tags=[]
    try:
        query="http://musicbrainz.org/ws/2/artist/" + mbid + "?inc=aliases+tags+url-rels&fmt=json"
#        print query
        AsyncHTTPClient.configure(None, defaults=dict(user_agent=user_agents[1]))
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(query)
#        print "response",response
        txt=response.body
        artinfo = json.loads(txt)
#        print "artinfo",artinfo
        if artinfo != None:
            country = artinfo.get('country','')
            if  artinfo['area'] != None:
                areaname = artinfo['area'].get('name','')
            rel=artinfo['relations']
            if rel != None:
                for r in rel:
                    type = r['type']
                    if type == 'wikipedia':
                        wpage = r['url']['resource'].split("/")[-1]
                        lang = wpage[2].split('.')[0]
                        summary = yield get_wikipedia(wpage)
                        summaries[lang] = summary
                    links.append((type,r['url']['resource']))
            atags=artinfo['tags']
            if atags != None:
                for t in atags:
                    name= t['name']
                    tags.append(name)
    except Exception as ex:
        print ex
    raise gen.Return((links,summaries,tags,country,areaname))


@gen.coroutine
def get_wikipedia(page,lng='en'):
    summary = ''
    try:
        params = {'prop': 'extracts','explaintext': '','exintro': ''}
        params['titles'] = page
        params['format'] = 'json'
        params['action'] = 'query'
        query = API_WIKI + urllib.urlencode(params)
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(query)
        txt=response.body
#        logger.info("get_wikipedia %s, %s",query, txt)
        winfo = json.loads(txt)
        pg =winfo['query']['pages'].keys()[0]
        summary = winfo['query']['pages'][pg]['extract']
        if summary.find('music')<0 and summary.find('song')<0 and summary.find('play')<0  and summary.find('sing')<0:
            summary = ''
    except Exception as ex:
        logger.info('get_wikipedia Exception %s',ex)
    raise gen.Return(summary)


@gen.coroutine
def get_lastfm(artist):
    summary = ""
    mbid = ""
    url = ""
    medimg = ""
    largeimg = ""
    tags = []
    try:
        artist=artist.replace(' ',"%20")
        query = "http://ws.audioscrobbler.com/2.0/?method=artist.search&artist=%s&api_key=8b31024f96a4698437d36f3b73c8ae83&format=json" % artist
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(query)
        txt=response.body
#        print txt
        artinfo = json.loads(txt)
        res= artinfo['results']['artistmatches']['artist']
        nm=res[0]['url'].split("/")[-1]
        query = "http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist=%s&api_key=8b31024f96a4698437d36f3b73c8ae83&format=json" % nm
        response = yield http_client.fetch(query)
        txt=response.body
        artinfo = json.loads(txt)
        if  type(artinfo) is dict:
            if artinfo.has_key('artist'):
                if artinfo['artist'].has_key('bio'):
                    summary = artinfo['artist']['bio'].get('summary','')
                mbid = artinfo['artist'].get('mbid','')
                url = artinfo['artist'].get('url','')
                for img in artinfo['artist']['image']:
                    if img['size']=='medium':
                        medimg = img.get('#text','')
                    if img['size']=='large':
                        largeimg = img.get('#text','')
                if artinfo['artist'].has_key('tag'):
                    for t in artinfo['artist']['tag']:
                        name = t.get('name','')
                        if name !=None and name !='':
                            tags.append(name)

      #  txt = unicode(summary,"iso-8859-1")
       #Clear text from links
        h = html2text.HTML2Text()
        h.ignore_images = True
        h.ignore_links = True
        summary = h.handle(summary)
    except Exception as ex:
        print "Exception get_lastfm",ex
    raise gen.Return((summary,mbid,url,medimg,largeimg,tags))


@gen.coroutine
def save_artists_img(db,allartists):
    for art in allartists:
        if art[0] != None and art[1]!= "":
            yield save_artist_img(db,art[1],art[0])


@gen.coroutine
def save_artist_img(db,bit_url,artist_id):
    #save artist image from bandsintown
    p = yield  db.artists.find_one({"_id": artist_id})
    if p != None:
        imgfolder = "static/img/artists/"
        smimgor = p.get('img_small','')
        mdimgor = p.get('img_medium','')
        smexist=mdexist=False
        if smimgor !='' and smimgor !='blank_art.jpg':
            smexist = os.path.isfile(imgfolder+smimgor)
        if mdimgor !='' and mdimgor !='blank_art.jpg':
            mdexist = os.path.isfile(imgfolder+mdimgor)
        if smexist==False or mdexist==False:
            #search image if they don't exist
            http_client = AsyncHTTPClient()
            response = yield http_client.fetch(bit_url)
            smimg = 'blank_art.jpg'
            mdimg = 'blank_art.jpg'
            try:
                txt=response.body
                soup = BeautifulSoup(txt)
                scc= soup.find('img',{"class":"sidebar-image"})
                if scc:
                    img_url = scc.get('src','')
                    if img_url != '' and img_url.find('1358885582') < 0:    #check a blank image
                        img_url = clearlink(img_url)
        #                saveimage(img_url, imgfolder + str(artist_id) + "_large.jpg")
                        img_url_s = img_url.replace("large","small")
                        if saveimage(img_url_s, imgfolder + str(artist_id) + "_small.jpg"):
                            smimg = str(artist_id) + "_small.jpg"
                        img_url_m = img_url.replace("large","medium")
                        if saveimage(img_url_m, imgfolder + str(artist_id) + "_medium.jpg"):
                            mdimg = str(artist_id) + "_medium.jpg"
                 #           print "save image",  str(artist_id)  + "_medium.jpg"
            except Exception as ex:
                print ex
            p['img_small'] = smimg
            p['img_medium'] = mdimg
            res = yield db.artists.update({"_id": artist_id},p)
       #     print "save image in db ", p['img_medium']


@gen.coroutine
def search_bing(artist):
    #Get first page from Bing and get information from facebook
    artist = '+'.join(artist.split())
#    artist += "+music"
    query = "http://www.bing.com/search?q=%s" % artist
#    logger.info('search_bing %s',artist)
    links=[]
    info={}
    user_agent = random.choice(user_agents)
    AsyncHTTPClient.configure(None, defaults=dict(user_agent=user_agent))
    try:
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(query)
        txt = response.body
        soup = BeautifulSoup(txt)
        elems= soup.find_all('li',{"class":"b_algo"})
        for e in elems:
            lnk = e.find('a')
            if lnk != None:
                href=lnk.get('href','')
                if href.find('www.facebook.com')>0:
                    info = get_FB_info(href,info)
                if href.find('jango.com')>0:
                    info['jango'] = href
                if href.find('mtv.com')>0:
                    info['mtv'] = href
                if info.get('lastfm','')=='' and  href.find('last.fm') > 0:
                    info['lastfm'] = href
                if info.get('wiki','')=='' and  href.find('wikipedia.org') > 0:
                    info['wiki'] = href
                links.append(href)
    except Exception as ex:
        logger.info('search_bing Exception %s',ex)
#    logger.info('search_bing links %s',str(links))
    raise gen.Return(info)


def get_FB_info(fblink,info):
    if fblink.find('facebook.com') < 0:
        return info
    user_id=fblink.split("/")[-1]
    api_query = urllib.urlopen('http://graph.facebook.com/'+user_id)
    dct = json.loads(api_query.read())
    info['name'] = dct.get('name','')
    firstname=info['name'].split()[0]
    info['bio'] = dct.get('bio','')
    if info['bio'] == '':
        about = dct.get('about','')
        if about.find(firstname)>=0:
            info['bio'] = about
    if info['bio'] == '':
        info['bio'] = dct.get('personal_info','')
    if info['bio'] == '':
        info['bio'] = dct.get('description','')
    if dct.has_key('cover'):
        info['imglink'] = dct['cover'].get('source','')
    info['website'] = dct.get('website','')
    info['hometown'] = dct.get('hometown','')
    info['areaname'] = dct.get('current_location','')
    info['category'] = dct.get('category','')
    info['genre'] = dct.get('genre','')
    info['fblink'] = dct.get('link','')
    return info


@gen.coroutine
def city_by_ip(ip):
    city = ""
    region =""
    country =""
    loc = ""
    try:
        query="http://ipinfo.io/" + ip + "/json"
        http_client = AsyncHTTPClient()
        response = yield http_client.fetch(query)
        txt=response.body
        info = json.loads(txt)
        city = info.get('city','')
        region = info.get('region','')
        country = info.get('country','')
        loc = info.get('loc','')
    except Exception as ex:
        print ex
    raise gen.Return((city,region,country,loc))

@gen.coroutine
def address_by_coordinates(lat,lng):
    address = ""
    try:
        if (lat,lng) in cache_coord_address:
            address = cache_coord_address[(lat,lng)]
        else:
            query="http://maps.googleapis.com/maps/api/geocode/json?latlng=%s,%s&sensor=false" % (lat,lng)
            http_client = AsyncHTTPClient()
            response = yield http_client.fetch(query)
            txt=response.body
            info = json.loads(txt)
            if "results" in info and len(info["results"]) > 0 and "formatted_address" in info["results"][0]:
                address = info["results"][0]["formatted_address"]
                cache_coord_address[(lat,lng)] = address
    except Exception as ex:
        print(ex)
    raise gen.Return(address)
