#-------------------------------------------------------------------------------
# Name:        musutils.py
# Purpose:    Utils for livemusic project
#
# Author:      Serge
#
# Created:     20/10/2014
# Copyright:   (c) Sydney 2014
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import urllib
import imghdr
#from bson.objectid import ObjectId
import re
from math import radians, sqrt, sin, cos, atan2
import copy
#import bs4,html2text


def city_name(db,city_id):
    #get city name by city id in Mongo database
    if city_id=="":
        return ""
    try:
        ct = db.cities.find_one({'_id': ObjectId(city_id)})
        if ct!=None and type(ct) is dict:
            return ct['city'] +', '+ct['country']
    except Exception as ex:
        print ex
    return ""


def city_location(db,address):
    #get longitude,latitude by address from database or from Google
    longitude = 0
    latitude = 0
    if address == None or address == "":
        return longitude,latitude
    cc= address.split(',')
    if len(cc) == 2 and len(cc[1].strip()) == 2:
        city = cc[0].strip()
        country = cc[1].strip().upper()
        rcity="^"+city+"$"
        try:
            ct = db.cities.find_one({'city': {'$regex': rcity, '$options': 'i'},'country': country})
 #           print "type(ct)",type(ct)
            if ct!=None and type(ct) is dict:
                longitude = ct['loc'][0]
                latitude = ct['loc'][1]
            else:
                longitude,latitude = google_location(address)
        except Exception as ex:
            print ex

    else:
        longitude,latitude =google_location(address)
    return longitude,latitude


def city_point(db,city_id):
    #get longitude and latitude by city ID in Mongo database
    longitude = 0
    latitude = 0
    if city_id=="":
        return longitude,latitude
    try:
        ct = db.cities.find_one({'_id': ObjectId(city_id)})

        if ct!=None and type(ct) is dict:
            longitude = ct['loc'][0]
            latitude = ct['loc'][1]
    except Exception as ex:
        print ex
    return longitude,latitude


def google_location(address):
    #get longirude and latitude by address from Google maps
    try:
        ad = address.replace(',',' ')
        ad = ad.split()
        adqr = '+'.join(ad)
        query="http://maps.googleapis.com/maps/api/geocode/json?address=" + adqr + "&sensor=false"
        headers['User-agent'] = random.choice(user_agents)
        response = requests.get(query, headers=headers, timeout=25)
        if response != 'Error' and response.ok:
            txt=response.content
            geoinfo = json.loads(txt)
            res=geoinfo['results']
            loc=res[0]['geometry']['location']
            return loc['lng'],loc['lat']
    except Exception as ex:
        print ex
    return 0,0

def cmp(a, b):
    #Compare  objects (dictionary,lists,strings,...)
    if type(a) is not type(b):
        return 1
    if type(a) is dict:
        keya = set(a.keys())
        keyb = set(b.keys())
        if len(keya - keyb)+len(keyb - keya) > 0:
            return 1
        for k in a:
            if cmp(a[k],b[k]):
                return 1
        return 0
    elif type(a) is list or type(a) is tuple:
        if len(a) != len(b): return 1
        for i,r in enumerate(a):
            if cmp(r,b[i]) == 1:
                return 1
        return 0
    else:
        if a == b:
            return 0
    return 1

def dict_merge(dct, merge_dct):
    """ Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
    updating only top-level keys, dict_merge recurses down into dicts nested
    to an arbitrary depth, updating keys. The ``merge_dct`` is merged into
    ``dct``.
    :param dct: dict onto which the merge is executed
    :param merge_dct: dct merged into dct
    :return: result dictonary
    """
    dctn = copy.deepcopy(dct)
    for k, v in merge_dct.items():
        if (k in dctn and isinstance(dctn[k], dict)
                and isinstance(merge_dct[k], dict)):
            dict_merge(dct[k], merge_dct[k])
        else:
            dctn[k] = merge_dct[k]
    return dctn

def toint(s):
    #Convert  value to int
    if s==None:
        return 0
    try:
        t=int(s)
        return t
    except ValueError:
        return 0

def tofloat(s):
    #convert value to float
    if s==None:
        return None
    try:
        t=float(s)
        return t
    except ValueError:
        return None


def isemail(email):
    #Check email address
    if re.match(r"^[a-zA-Z0-9._%-+]+@[a-zA-Z0-9._%-]+.[a-zA-Z]{2,6}$",email):
        return True
    else:
        return False

def geocalc( lon1,lat1, lon2, lat2):
    #Calculate distance between two points
    lat1 = radians(lat1)
    lon1 = radians(lon1)
    lat2 = radians(lat2)
    lon2 = radians(lon2)
    dlon = lon1 - lon2
    EARTH_R = 6372.8
    y = sqrt(
        (cos(lat2) * sin(dlon)) ** 2
        + (cos(lat1) * sin(lat2) - sin(lat1) * cos(lat2) * cos(dlon)) ** 2
        )
    x = sin(lat1) * sin(lat2) + cos(lat1) * cos(lat2) * cos(dlon)
    c = atan2(y, x)
    return EARTH_R * c


def saveimage(url,imgfile):
    urllib.urlretrieve(url, imgfile)
    if imghdr.what(imgfile)==None:
        os.remove(imgfile)
        print "It's not an image",url
        return False
    return True


def clearlink(url):
    f = url.find('?')
    if f > 7 :
        url = url[:f]
    return url


def gettxt(elem):
    txt=""
    if type(elem) is bs4.Tag :
        txt=elem.get_text()
    return txt.strip()